﻿-- =============================================
-- Create index basic template
-- =============================================
create index <index_name, ind_test>
on <schema_name, main>.<table_name, Address> 
(
	<column_name1, PostalCode>
)