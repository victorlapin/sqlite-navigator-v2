﻿-- =============================================
-- Create View template
-- =============================================
create view <schema_name, main>.<view_name, Top10Sales> as
<select_statement, select * from Sales.SalesOrderHeader order by TotalDue desc limit 10>