﻿--==========================================================================
-- Add column template
--==========================================================================
alter table <schema_name, main>.<table_name, sample_table>
	add <new_column_name, column3> <new_column_datatype, integer> <new_column_nullability, null>