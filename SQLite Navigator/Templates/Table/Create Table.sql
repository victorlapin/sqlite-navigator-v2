﻿-- =========================================
-- Create table template
-- =========================================
create table <schema_name, main>.<table_name, sample_table>
(
	<columns_in_primary_key, , c1> <column1_datatype, integer> <column1_nullability, not null>, 
	<column2_name, c2> <column2_datatype, char(10)> <column2_nullability, null>, 
	<column3_name, c3> <column3_datatype, datetime> <column3_nullability, null>, 
    constraint <contraint_name, PK_sample_table> primary key (<columns_in_primary_key, c1>)
)