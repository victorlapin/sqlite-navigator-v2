﻿using System.Windows;
using SQLiteNavigator.Classes.Helpers;

namespace SQLiteNavigator.Windows
{
    public partial class PasswordWindow : Window
    {
        public PasswordWindow()
        {
            InitializeComponent();

            lblAppName.Text = SettingsHelper.ApplicationName;
        }
        
        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }
    }
}