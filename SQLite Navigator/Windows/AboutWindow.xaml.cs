﻿using System.Windows;
using System;
using SQLiteNavigator.Classes;
using System.Reflection;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using SQLiteNavigator.Classes.Helpers;
using System.Diagnostics;

namespace SQLiteNavigator.Windows
{
    public partial class AboutWindow : Window
    {
        private class KeyValueEntry
        {
            public string Key { get; set; }
            public string Value { get; set; }
            public string Path { get; set; }
        }

        public AboutWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Title = String.Format("About {0}", SettingsHelper.ApplicationName);
            this.lblAppName.Text = SettingsHelper.ApplicationName;
            Assembly assembly = Assembly.GetExecutingAssembly();
            this.lblDescription.Text = GetAssemblyDescription(assembly);
            this.lblCopyright.Text = GetAssemblyCopyright(assembly);
        }

        #region GetAttributes

        private string GetAssemblyDescription(Assembly assembly)
        {
            object[] attributes = assembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
            if (attributes.Length == 0)
            {
                return String.Empty;
            }
            return ((AssemblyDescriptionAttribute)attributes[0]).Description;
        }

        private string GetAssemblyCopyright(Assembly assembly)
        {
            object[] attributes = assembly.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
            if (attributes.Length == 0)
            {
                return String.Empty;
            }
            return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
        }

        #endregion

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((tabControl.SelectedItem as TabItem).Header.Equals("General") && gridGeneral.ItemsSource == null)
            {
                //fill General tab
                ObservableCollection<KeyValueEntry> general = new ObservableCollection<KeyValueEntry>();
                general.Add(new KeyValueEntry() { Key = "Application Version", Value = Assembly.GetExecutingAssembly().GetName().Version.ToString() });
                general.Add(new KeyValueEntry() { Key = "Application Codename", Value = SettingsHelper.ApplicationCodename });
                general.Add(new KeyValueEntry() { Key = "Installation Directory", Value = AppDomain.CurrentDomain.BaseDirectory });
                general.Add(new KeyValueEntry() { Key = "Common Config Directory", Value = SettingsHelper.CommonConfigDir });
                general.Add(new KeyValueEntry() { Key = "User Config Directory", Value = SettingsHelper.UserConfigDir });
                general.Add(new KeyValueEntry() { Key = "SQLite Database Engine Version", Value = Workbench.Self.Solution.Connection.ServerVersion });
                general.Add(new KeyValueEntry() { Key = ".NET Framework Version", Value = Environment.Version.ToString(3) });
                general.Add(new KeyValueEntry() { Key = "OS Version", Value = Environment.OSVersion.Version.ToString(3) });
                gridGeneral.ItemsSource = new ReadOnlyObservableCollection<KeyValueEntry>(general);
            }
            else if ((tabControl.SelectedItem as TabItem).Header.Equals("Components") && gridVersions.ItemsSource == null)
            {
                //fill Components tab
                List<AssemblyName> assemblies = Assembly.GetExecutingAssembly().GetReferencedAssemblies().ToList();
                assemblies.Sort(delegate(AssemblyName x, AssemblyName y) { return x.Name.CompareTo(y.Name); });
                var versions = assemblies.AsEnumerable()
                    .Select(a => new KeyValueEntry()
                            {
                                Key = a.Name,
                                Value = a.Version.ToString(),
                                Path = Assembly.Load(a).Location
                            });
                gridVersions.ItemsSource = versions;
            }
            else if ((tabControl.SelectedItem as TabItem).Header.Equals("Credits") && gridCredits.ItemsSource == null)
            {
                //fill Credits tab
                ObservableCollection<KeyValueEntry> credits = new ObservableCollection<KeyValueEntry>();
                credits.Add(new KeyValueEntry() { Key = "SQLite.NET Data Provider", Value = "SQLite Development Team" });
                credits.Add(new KeyValueEntry() { Key = "AvalonEdit control", Value = "ICSharpCode Team" });
                credits.Add(new KeyValueEntry() { Key = "SharpTreeView control", Value = "ICSharpCode Team" });
                credits.Add(new KeyValueEntry() { Key = "Extended WPF Toolkit Community Edition", Value = "Xceed Team" });
                credits.Add(new KeyValueEntry() { Key = "Application icon", Value = "Malcer Quaid" });
                gridCredits.ItemsSource = new ReadOnlyObservableCollection<KeyValueEntry>(credits);
            }
        }

        private void grid_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        private void btnLicense_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("http://opensource.org/licenses/ms-pl");
        }
    }
}
