﻿using System;
using System.Windows;
using SQLiteNavigator.Classes.Helpers;

namespace SQLiteNavigator.Windows
{
    public partial class ExceptionWindow : Window
    {
        public ExceptionWindow(Exception e)
        {
            InitializeComponent();

            this.lblAppName.Text = SettingsHelper.ApplicationName;
            this.edtTrace.Text = e.ToString();
        }
        
        private void BtnCopy_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(edtTrace.Text, TextDataFormat.Text);
        }
        
        private void BtnRestart_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }
    }
}