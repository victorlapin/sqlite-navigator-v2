﻿using System;
using ICSharpCode.AvalonEdit.Highlighting;
using System.IO;
using System.Reflection;
using System.Xml;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using ICSharpCode.AvalonEdit.Folding;
using ICSharpCode.AvalonEdit.CodeCompletion;
using System.Collections.Generic;
using SQLiteNavigator.Classes.IntelliSense;
using System.Windows.Media.Imaging;
using SQLiteNavigator.Classes.Folding;

namespace SQLiteNavigator.Classes.Helpers
{
    public static class ResourceHelper
    {        
        public static string GetResourcePath(string resName, int size = 16)
        {
            return String.Format("pack://application:,,,/SQLiteNavigator;component/Resources/{0}/{1}.png", size, resName);
        }

        private static Dictionary<string, BitmapImage> _glyphs = new Dictionary<string, BitmapImage>();
        public static BitmapImage GetResource(string resName, int size = 16)
        {
            if (String.IsNullOrEmpty(resName))
                return null;

            if (!_glyphs.ContainsKey(resName))
            {
                var bitmap = new BitmapImage(new Uri(GetResourcePath(resName, size), UriKind.RelativeOrAbsolute));
                if (bitmap != null)
                {
                    _glyphs.Add(resName, bitmap);
                }
                else
                    return null;
            }
            
            return _glyphs[resName];
        }

        private static IHighlightingDefinition _sqliteDefinition = null;
        public static IHighlightingDefinition SqliteDefinition
        {
            get 
            {
                if (_sqliteDefinition == null)
                {
                    using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("SQLiteNavigator.Sqlite.xshd"))
                    {
                        using (XmlTextReader reader = new XmlTextReader(stream))
                            _sqliteDefinition = HighlightingLoader.Load(reader, HighlightingManager.Instance);
                    }                        
                }

                return _sqliteDefinition;
            }
        }

        private static AbstractFoldingStrategy _sqlFoldingStrategy = new SqliteFoldingStrategy();
        public static AbstractFoldingStrategy SqlFoldingStrategy
        {
            get 
            {
                return _sqlFoldingStrategy;
            }
        }
    }
}