﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Xceed.Wpf.AvalonDock;
using System.Xml;

namespace SQLiteNavigator.Classes.Helpers
{
    public static class SettingsHelper
    {
        public static string ConfigDockFileName { get { return "sqlitenav.dock.config"; } }
        public static string ConfigFileName { get { return "sqlitenav.config"; } }        
        public static string SessionsFileName { get { return "sessions.xml"; } }
        public static string ApplicationCodename { get { return "Arabella"; } }
        public static string ApplicationName { get { return "SQLite Navigator"; } }
        public static bool IsCrashing { get; set; }
        
        static SettingsHelper()
        {
            IsCrashing = false;
        }

        #region ConfigDirs
        
        private static string userConfigDir = Path.Combine(Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), ApplicationName), ApplicationCodename);
        private static string commonConfigDir = Path.Combine(Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), ApplicationName), ApplicationCodename);
        
        public static string UserConfigDir { get { return userConfigDir; } }
        public static string CommonConfigDir { get { return commonConfigDir; } }

        private static void ValidateDirectory(string dir)
        {
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
        }
        
        #endregion
        
        #region Recent Sessions
        
        public static void LoadRecentSessions(ObservableCollection<RecentSession> target)
        {
            string fileName = Path.Combine(UserConfigDir, SessionsFileName);
            if (File.Exists(fileName))
            {
                XDocument xml = XDocument.Load(fileName);
                IEnumerable<XElement> sessions = xml.Element("SQLiteNav.RecentSessions").Elements("Session");
                foreach (XElement session in sessions)
                {
                    RecentSession item = new RecentSession()
                    {
                        Name = session.Attribute("Name").ToString(),
                        Path = session.Attribute("Path").ToString(),
                        LastAccessed = Convert.ToDateTime(session.Attribute("LastAccessed").ToString()),
                        IsSolution = Convert.ToBoolean(session.Attribute("IsSolution").ToString())
                    };
                    target.Add(item);
                }
            }
        }
        
        public static void SaveRecentSessions(ObservableCollection<RecentSession> source)
        {
            XDocument xml = new XDocument(
                new XDeclaration("1.0", Encoding.UTF8.HeaderName, "yes"),
                new XElement("SQLiteNav.RecentSessions",
                             source.Select(s => new XElement("Session",
                                                             new XAttribute("Name", s.Name),
                                                             new XAttribute("Path", s.Path),
                                                             new XAttribute("LastAccessed", s.LastAccessed),
                                                             new XAttribute("IsSolution", s.IsSolution))).Take(10)
                            )
               );
            ValidateDirectory(UserConfigDir);
            xml.Save(Path.Combine(UserConfigDir, SessionsFileName));
        }
        
        #endregion
        
        #region Docking
        
        public static void SaveDockSettings(DockingManager manager)
        {
                manager.Layout.RootPanel.WriteXml(new XmlTextWriter(Path.Combine(UserConfigDir, ConfigDockFileName), Encoding.UTF8));
        }
        
        public static void RestoreDockSettings(DockingManager manager)
        {
            using (FileStream fs = new FileStream(Path.Combine(UserConfigDir, ConfigDockFileName), FileMode.Open))
                manager.Layout.RootPanel.ReadXml(new XmlTextReader(fs));
        }
        
        #endregion
    }
}
