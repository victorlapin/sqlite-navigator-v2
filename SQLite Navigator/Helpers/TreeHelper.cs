﻿using SQLiteNavigator.Classes.Nodes;

namespace SQLiteNavigator.Helpers
{
    public static class TreeHelper
    {
        public static TreeNode GetCatalog(TreeNode node)
        {
            if (node == null) return null;
            else
            {
                while (node.Parent != null)
                {
                    if (node != null)
                    {
                        if (node is CatalogTreeNode || node is AttachedCatalogTreeNode)
                            return node;
                        else
                            node = node.Parent as TreeNode;
                    }
                }
                return null;
            }
        }

        public static TreeNode GetTable(TreeNode node)
        {
            if (node == null) return null;
            else
            {
                while (node.Parent != null)
                {
                    if (node != null)
                    {
                        if (node is TableTreeNode)
                            return node;
                        else
                            node = node.Parent as TreeNode;
                    }
                }
                return null;
            }
        }
    }
}
