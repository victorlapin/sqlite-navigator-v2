﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;
using SQLiteNavigator.Classes;
using SQLiteNavigator.Windows;

namespace SQLiteNavigator
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static CompositionContainer compositionContainer;

        public static CompositionContainer CompositionContainer
        {
            get { return compositionContainer; }
        }

        public App()
        {
            AggregateCatalog catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(App).Assembly));
            string pluginsPath = Path.Combine(".","Plugins");
            catalog.Catalogs.Add(new DirectoryCatalog(".", "*.dll"));
            if (Directory.Exists(pluginsPath))
                catalog.Catalogs.Add(new DirectoryCatalog(pluginsPath, "*.Plugin.dll"));

            compositionContainer = new CompositionContainer(catalog);

            InitializeComponent();
        }
        
        private void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            ExceptionWindow window = new ExceptionWindow(e.Exception);
            if (Application.Current.MainWindow.IsLoaded)
                window.Owner = Application.Current.MainWindow;
            if (window.ShowDialog().Value)
            {
                    Workbench.Self.Solution.FileName = Path.Combine(Path.GetTempPath(), Solution.RecoveryFileName);
                    Workbench.Self.Solution.SaveSolution();             
                    Process.Start(new ProcessStartInfo(Assembly.GetExecutingAssembly().Location, String.Format(@"""{0}""", Workbench.Self.Solution.FileName)));
            }
            Application.Current.Shutdown();
        }
    }
}
