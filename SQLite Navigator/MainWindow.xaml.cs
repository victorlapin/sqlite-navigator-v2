﻿using System;
using System.ComponentModel.Composition;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Xceed.Wpf.AvalonDock;
using SQLiteNavigator.Classes;
using SQLiteNavigator.Commands;
using SQLiteNavigator.Metadata;
using System.Windows.Interop;
using SQLiteNavigator.ViewModels;
using SQLiteNavigator.Classes.Helpers;
using Xceed.Wpf.AvalonDock.Layout;
using SQLiteNavigator.Classes.Info;

namespace SQLiteNavigator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static MainWindow instance;

        public static MainWindow Instance
        {
            get { return instance; }
        }

        public DockingManager DockingManager
        {
            get { return dockingManager; }
        }

        #region Toolbar extensibility
        
        [ImportMany("ToolbarCommand", typeof(ICommand))]
        Lazy<ICommand, IToolbarCommandMetadata>[] toolbarCommands = null;

        private void InitToolbar()
        {
            foreach (var commandGroup in toolbarCommands.OrderBy(c => c.Metadata.ToolbarOrder).GroupBy(c => c.Metadata.ToolbarCategory))
            {
                if (tlbMain.Items.Count > 0)
                    tlbMain.Items.Add(new Separator());
                foreach (var command in commandGroup)
                {
                    tlbMain.Items.Add(MakeToolbarItem(command));
                }
            }
        }

        Button MakeToolbarItem(Lazy<ICommand, IToolbarCommandMetadata> command)
        {
            StackPanel content = new StackPanel
            {
                Orientation = Orientation.Horizontal
            };
            content.Children.Add(
                new Image
                {
                    Width = 16,
                    Height = 16,
                    Stretch = Stretch.None,
                    Source = ResourceHelper.GetResource(command.Metadata.ToolbarIcon)
                }
               );
            if (!String.IsNullOrEmpty(command.Metadata.ToolbarText))
            {
                content.Children.Add(
                    new TextBlock
                    {
                        Margin = new Thickness(4,0,0,0),
                        VerticalAlignment = VerticalAlignment.Center,
                        Text = command.Metadata.ToolbarText
                    }
                   );
            }
            
            return new Button
            {
                Command = CommandWrapper.Unwrap(command.Value),
                ToolTip = command.Metadata.ToolTip,
                Content = content
            };
        }

        #endregion

        #region Main Menu extensibility
        [ImportMany("MainMenuCommand", typeof(ICommand))]
        Lazy<ICommand, IMainMenuCommandMetadata>[] mainMenuCommands = null;

        void InitMainMenu()
        {
            foreach (var topLevelMenu in mainMenuCommands.OrderBy(c => c.Metadata.MenuOrder).GroupBy(c => c.Metadata.Menu))
            {
                var topLevelMenuItem = mainMenu.Items.OfType<MenuItem>().FirstOrDefault(m => (m.Header as string) == topLevelMenu.Key);
                foreach (var category in topLevelMenu.GroupBy(c => c.Metadata.MenuCategory))
                {
                    if (topLevelMenuItem == null)
                    {
                        topLevelMenuItem = new MenuItem();
                        topLevelMenuItem.Header = topLevelMenu.Key;
                        mainMenu.Items.Insert(mainMenu.Items.Count - 2, topLevelMenuItem);
                    }
                    else if (topLevelMenuItem.Items.Count > 0)
                    {
                        topLevelMenuItem.Items.Add(new Separator());
                    }
                    foreach (var entry in category)
                    {
                        MenuItem menuItem = new MenuItem();
                        menuItem.Focusable = false;
                        menuItem.Command = CommandWrapper.Unwrap(entry.Value);
                        if (!string.IsNullOrEmpty(entry.Metadata.Header))
                            menuItem.Header = entry.Metadata.Header;
                        if (!string.IsNullOrEmpty(entry.Metadata.MenuIcon))
                        {
                            menuItem.Icon = new Image
                            {
                                Width = 16,
                                Height = 16,
                                Source = ResourceHelper.GetResource(entry.Metadata.MenuIcon)
                            };
                        }
                        if (entry.Metadata.KeyGesture != null)
                        {
                            string gesture = entry.Metadata.KeyGesture;
                            KeyGestureConverter converter = new KeyGestureConverter();
                            if (converter.IsValid(gesture))
                            {
                                menuItem.InputGestureText = gesture;
                                this.InputBindings.Add(new KeyBinding(entry.Value, (KeyGesture)converter.ConvertFromString(gesture)));
                            }
                        }
                        topLevelMenuItem.Items.Add(menuItem);
                    }
                }
            }

            //delete empty entries
            int i = 0;
            while (i != mainMenu.Items.Count - 1)
            {
                if (!(mainMenu.Items[i] as MenuItem).HasItems)
                    mainMenu.Items.RemoveAt(i);
                else
                    i++;
            }
        }
        #endregion
        
        #region DockContents extensibility
        
        [ImportMany("DockContent", typeof(PaneViewModel))]
        Lazy<PaneViewModel, IDockContentMetadata>[] dockContents = null;
        
        private void InitDockContents()
        {
            foreach (var content in dockContents)
            {
                if (content.Metadata.Anchor != AnchorableShowStrategy.Most)
                {
                    ToolViewModel view = content.Value as ToolViewModel;
                    view.Tag = content.Metadata;
                    Workbench.Self.Tools.Add(view);
                }
            }
        }
        
        #endregion

        #region PropertiesInfo extensibility

        [ImportMany("PropertiesInfo", typeof(IBaseInfo))]
        Lazy<IBaseInfo, IPropertiesInfoMetadata>[] propInfos = null;

        private void InitPropertiesInfo()
        {
            PropertiesInfoFactory.SetSource(propInfos);
        }

        #endregion

        #region ContextMenu extensibility

        [ImportMany("ContextMenuCommand", typeof(ICommand))]
        Lazy<ICommand, IContextMenuCommandMetadata>[] contextCommands = null;

        private void InitContextMenu()
        {
            ContextMenuFactory.SetSource(contextCommands);
        }

        #endregion

        public MainWindow()
        {
            HwndSource.DefaultAcquireHwndFocusInMenuMode = false;
            instance = this;
            InitializeComponent();

            tlbMain.Loaded += new RoutedEventHandler(Extensions.Toolbar_Loaded);
            App.CompositionContainer.ComposeParts(this);
            this.DataContext = Workbench.Self;

            //setting up connection
            Workbench.Self.Solution.Connection.StateChange += new StateChangeEventHandler(Connection_StateChange);
            
            Workbench.Self.WriteOutput("info", "Application started");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //load plugin exported main menu items
            InitMainMenu();
            //load plugin exported toolbar items
            InitToolbar();
            //load plugin exported dock contents
            InitDockContents();
            //load plugin exported property views
            InitPropertiesInfo();
            //load plugin exported context menu items for object explorer
            InitContextMenu();
            
            //filling recent sessions
            SettingsHelper.LoadRecentSessions(Workbench.Self.RecentSessions);
            new ViewStartPageCommand().Execute(null);
        }

        private void Connection_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            ObjectExplorerViewModel oe = Workbench.Self.FindTool<ObjectExplorerViewModel>();
            switch (e.CurrentState)
            {
                case ConnectionState.Closed:
                    lblConnectionState.Text = "Disconnected";

                    if (oe != null)
                        oe.ClearObjectExplorer();
                    PropertiesViewModel p = Workbench.Self.FindTool<PropertiesViewModel>();
                    if (p != null)
                        p.ShowProperties(String.Empty, null);
                    Workbench.Self.WriteOutput("ok", "Session closed");
                    break;
                case ConnectionState.Open:
                    lblConnectionState.Text = "Connected";

                    if (oe != null)
                        oe.PopulateObjectExplorer();
                    Workbench.Self.WriteOutput("ok", "Connected");
                    break;
                default:
                    lblConnectionState.Text = e.CurrentState.ToString();
                    break;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Workbench.Self.Solution.Connection.State == ConnectionState.Open)
            {
                Workbench.Self.Solution.Connection.Close();
            }
        }
    }
}
