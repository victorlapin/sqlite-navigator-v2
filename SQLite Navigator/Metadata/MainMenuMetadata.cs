﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Input;

namespace SQLiteNavigator.Metadata
{
    public interface IMainMenuCommandMetadata
    {
        string MenuIcon { get; }
        string Header { get; }
        string Menu { get; }
        string MenuCategory { get; }
        string KeyGesture { get; }
        double MenuOrder { get; }
    }

    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ExportMainMenuCommandAttribute : ExportAttribute, IMainMenuCommandMetadata
    {
        public ExportMainMenuCommandAttribute()
            : base("MainMenuCommand", typeof(ICommand))
        {
        }

        public string MenuIcon { get; set; }
        public string Header { get; set; }
        public string Menu { get; set; }
        public string MenuCategory { get; set; }
        public string KeyGesture { get; set; }
        public double MenuOrder { get; set; }
    }
}
