﻿using System;
using System.ComponentModel.Composition;
using Xceed.Wpf.AvalonDock.Layout;
using SQLiteNavigator.ViewModels;

namespace SQLiteNavigator.Metadata
{
    public interface IDockContentMetadata
    {
        AnchorableShowStrategy Anchor { get; }
    }
    
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple=false)]
    public class ExportDockContentAttribute: ExportAttribute, IDockContentMetadata
    {
        public ExportDockContentAttribute()
            : base("DockContent", typeof(PaneViewModel))
        {

        }
        
        public AnchorableShowStrategy Anchor { get; set; }
    }
}
