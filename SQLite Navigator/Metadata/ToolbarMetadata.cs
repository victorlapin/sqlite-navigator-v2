﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Input;

namespace SQLiteNavigator.Metadata
{
    public interface IToolbarCommandMetadata
    {
        string ToolbarIcon { get; }
        string ToolTip { get; }
        string ToolbarCategory { get; }
        string ToolbarText { get; }
        double ToolbarOrder { get; }
    }
    
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple=false)]
    public class ExportToolbarCommandAttribute : ExportAttribute, IToolbarCommandMetadata
    {
        public ExportToolbarCommandAttribute()
            : base("ToolbarCommand", typeof(ICommand))
        {
        }
        
        public string ToolTip { get; set; }
        public string ToolbarIcon { get; set; }
        public string ToolbarCategory { get; set; }
        public string ToolbarText { get; set; }
        public double ToolbarOrder { get; set; }
    }
}
