﻿using System;
using System.ComponentModel.Composition;
using System.Windows.Input;

namespace SQLiteNavigator.Metadata
{
    public interface IContextMenuCommandMetadata
    {
        string MenuIcon { get; }
        string Header { get; }
        string MenuCategory { get; }
        double MenuOrder { get; }
        string NodeClassName { get; }
    }

    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]    
    public class ExportContextMenuCommandAttribute: ExportAttribute, IContextMenuCommandMetadata
    {
        public ExportContextMenuCommandAttribute()
            : base("ContextMenuCommand", typeof(ICommand))
        {
        }

        public string MenuIcon { get; set; }
        public string Header { get; set; }
        public string MenuCategory { get; set; }
        public double MenuOrder { get; set; }
        public string NodeClassName { get; set; }
    }
}
