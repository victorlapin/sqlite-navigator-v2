﻿using System;
using System.ComponentModel.Composition;
using SQLiteNavigator.Classes.Info;

namespace SQLiteNavigator.Metadata
{
    public interface IPropertiesInfoMetadata
    {
        string Contract { get; }
    }

    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class, AllowMultiple=false)]
    public class ExportPropertiesInfoAttribute : ExportAttribute, IPropertiesInfoMetadata
    {
        public ExportPropertiesInfoAttribute()
            : base("PropertiesInfo", typeof(IBaseInfo))
        {
            
        }

        public string Contract { get; set; }
    }
}
