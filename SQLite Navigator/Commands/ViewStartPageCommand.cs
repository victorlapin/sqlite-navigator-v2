﻿using SQLiteNavigator.Metadata;
using SQLiteNavigator.Classes;
using SQLiteNavigator.ViewModels;

namespace SQLiteNavigator.Commands
{
    [ExportMainMenuCommand(Header = "Start Page", Menu = "View", MenuIcon = "startpage", MenuOrder = 99)]
    public sealed class ViewStartPageCommand : SimpleCommand
    {
        public override void Execute(object parameter)
        {
            StartPageViewModel view = Workbench.Self.FindDocument<StartPageViewModel>();
            if (view == null)
            {
                view = new StartPageViewModel();
                Workbench.Self.Documents.Add(view);
            }
            view.IsActive = true;
        }
    }
}
