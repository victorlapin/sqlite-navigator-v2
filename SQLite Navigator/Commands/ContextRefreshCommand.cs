﻿using SQLiteNavigator.Metadata;
using SQLiteNavigator.Classes.Nodes;
using SQLiteNavigator.ViewModels;
using SQLiteNavigator.Classes;

namespace SQLiteNavigator.Commands
{
    [ExportContextMenuCommand(Header="Refresh", MenuCategory="general", MenuIcon="refresh", MenuOrder=99, NodeClassName="All")]
    public sealed class ContextRefreshCommand: SimpleCommand
    {
        public override void Execute(object parameter)
        {
            ObjectExplorerViewModel view = Workbench.Self.FindTool<ObjectExplorerViewModel>();
            if (view != null)
            {
                TreeNode node = view.GetSelectedItem() as TreeNode;
                if (node != null)
                    node.ReloadChildren();
            }
        }
    }
}
