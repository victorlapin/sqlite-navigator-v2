﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQLiteNavigator.Commands
{
    public abstract class OwnerCommand: SimpleCommand
    {
        public object Owner { get; private set; }

        public OwnerCommand(object owner)
        {
            this.Owner = owner;
        }
    }
}
