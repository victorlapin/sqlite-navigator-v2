﻿using SQLiteNavigator.Metadata;
using SQLiteNavigator.ViewModels;
using SQLiteNavigator.Classes;

namespace SQLiteNavigator.Commands
{
    [ExportMainMenuCommand(Header = "Template Explorer", Menu = "View", MenuIcon = "templateexplorer", MenuOrder = 3)]
    public sealed class ViewTemplateExplorerCommand : SimpleCommand
    {
        public override void Execute(object parameter)
        {
            TemplateExplorerViewModel view = Workbench.Self.FindTool<TemplateExplorerViewModel>();
            if (view != null)
            {
                view.IsVisible = true;
                view.IsActive = true;
            }
        }
    }
}
