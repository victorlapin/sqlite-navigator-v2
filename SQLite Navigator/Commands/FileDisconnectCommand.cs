﻿using System.Data;
using SQLiteNavigator.Classes;
using SQLiteNavigator.Metadata;

namespace SQLiteNavigator.Commands
{
    [ExportMainMenuCommand(Header="Close Connection", Menu="File", MenuIcon="disconnect", MenuOrder=3, MenuCategory="Connect")]
    [ExportToolbarCommand(ToolbarIcon="disconnect", ToolTip="Close Connection", ToolbarCategory="Connect", ToolbarOrder=3)]
    public sealed class FileDisconnectCommand: SimpleCommand
    {
        public override void Execute(object parameter)
        {
            Workbench.Self.Solution.Disconnect();
        }
        
        public override bool CanExecute(object parameter)
        {
            return (Workbench.Self.Solution.Connection.State != ConnectionState.Closed 
                    && Workbench.Self.Solution.Connection.State != ConnectionState.Broken);
        }
    }
}