﻿using SQLiteNavigator.ViewModels;

namespace SQLiteNavigator.Commands
{
    public sealed class ToolHideCommand: OwnerCommand
    {
        public ToolHideCommand(object owner)
            : base(owner)
        {

        }

        public override void Execute(object parameter)
        {
            (Owner as ToolViewModel).IsVisible = false;
        }
    }
}
