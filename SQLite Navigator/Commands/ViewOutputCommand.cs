﻿using SQLiteNavigator.Metadata;
using SQLiteNavigator.ViewModels;
using SQLiteNavigator.Classes;

namespace SQLiteNavigator.Commands
{
    [ExportMainMenuCommand(Header="Output", Menu="View", MenuIcon = "output", MenuOrder = 4)]
    public sealed class ViewOutputCommand: SimpleCommand
    {
        public override void Execute(object parameter)
        {
            OutputViewModel view = Workbench.Self.FindTool<OutputViewModel>();
            if (view != null)
            {
                view.IsVisible = true;
                view.IsActive = true;
            }
        }
    }
}
