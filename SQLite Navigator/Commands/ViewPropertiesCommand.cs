﻿using SQLiteNavigator.Metadata;
using SQLiteNavigator.ViewModels;
using SQLiteNavigator.Classes;

namespace SQLiteNavigator.Commands
{
    [ExportMainMenuCommand(Header = "Properties Window", Menu = "View", MenuIcon = "properties", MenuOrder = 2, KeyGesture = "F4")]
    public sealed class ViewPropertiesCommand : SimpleCommand
    {
        public override void Execute(object parameter)
        {
            PropertiesViewModel view = Workbench.Self.FindTool<PropertiesViewModel>();
            if (view != null)
            {
                view.IsVisible = true;
                view.IsActive = true;
            }
        }
    }
}
