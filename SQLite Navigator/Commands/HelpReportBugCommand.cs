﻿using System;
using SQLiteNavigator.Metadata;
using System.Diagnostics;

namespace SQLiteNavigator.Commands
{
    [ExportMainMenuCommand(Header="Report a Bug", Menu="Help", MenuOrder=3)]
    public sealed class HelpReportBugCommand: SimpleCommand
    {
        public override void Execute(object parameter)
        {
            Process.Start("http://trac.mysvn.ru/VictorLapin/SQLiteNavigator/newticket");
        }
    }
}
