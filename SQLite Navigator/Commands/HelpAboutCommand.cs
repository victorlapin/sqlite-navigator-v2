﻿using SQLiteNavigator.Metadata;
using SQLiteNavigator.Windows;

namespace SQLiteNavigator.Commands
{
    [ExportMainMenuCommand(Header = "About SQLite Navigator", Menu = "Help", MenuOrder = 999, MenuCategory="About")]
    public sealed class HelpAboutCommand : SimpleCommand
    {
        public override void Execute(object parameter)
        {
            AboutWindow window = new AboutWindow();
            window.Owner = MainWindow.Instance;
            window.ShowDialog();
        }
    }
}
