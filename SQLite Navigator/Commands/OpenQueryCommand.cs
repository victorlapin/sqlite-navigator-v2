﻿using SQLiteNavigator.Metadata;
using SQLiteNavigator.ViewModels;
using SQLiteNavigator.Classes;
using Microsoft.Win32;

namespace SQLiteNavigator.Commands
{
    [ExportMainMenuCommand(Header = "Open Query", Menu = "File", MenuIcon = "open", MenuOrder = 2, MenuCategory = "Editor", KeyGesture = "Ctrl+O")]
    [ExportToolbarCommand(ToolbarIcon = "open", ToolTip = "Open Query", ToolbarCategory = "Editor", ToolbarOrder = 2)]
    public sealed class OpenQueryCommand : SimpleCommand
    {
        public override void Execute(object parameter)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Query Files (*.sql)|*.sql";
            if (dialog.ShowDialog(MainWindow.Instance).Value)
            {
                new NewQueryCommand().Execute(dialog.FileName);
            }
        }
    }
}
