﻿using SQLiteNavigator.Classes;
using SQLiteNavigator.Metadata;

namespace SQLiteNavigator.Commands
{
    [ExportMainMenuCommand(Header = "Create In-Memory Database", Menu = "File", MenuIcon = "database_create", MenuOrder = 2, MenuCategory = "Connect")]
    [ExportToolbarCommand(ToolbarIcon = "database_create", ToolTip = "Create In-Memory Database", ToolbarCategory = "Connect", ToolbarOrder = 2)]
    public sealed class FileCreateDatabaseCommand : SimpleCommand
    {
        public override void Execute(object parameter)
        {
            Workbench.Self.Solution.Connect(":memory:", false, false);
        }
    }
}
