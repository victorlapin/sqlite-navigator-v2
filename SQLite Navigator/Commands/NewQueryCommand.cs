﻿using SQLiteNavigator.Metadata;
using SQLiteNavigator.ViewModels;
using SQLiteNavigator.Classes;

namespace SQLiteNavigator.Commands
{
    [ExportMainMenuCommand(Header = "New Query", Menu = "File", MenuIcon = "query_new", MenuOrder = 1, MenuCategory = "Editor", KeyGesture="Ctrl+N")]
    [ExportToolbarCommand(ToolbarIcon = "query_new", ToolTip = "New Query", ToolbarCategory = "Editor", ToolbarText = "New Query", ToolbarOrder = 1)]
    public sealed class NewQueryCommand: SimpleCommand
    {
        public override void Execute(object parameter)
        {
            EditorViewModel view = new EditorViewModel();
            Workbench.Self.Documents.Add(view);
            view.IsActive = true;

            if (parameter != null)
                view.Load(parameter.ToString());
        }
    }
}
