﻿using SQLiteNavigator.Metadata;
using SQLiteNavigator.ViewModels;
using SQLiteNavigator.Classes;

namespace SQLiteNavigator.Commands
{
    [ExportMainMenuCommand(Header = "Object Explorer", Menu = "View", MenuIcon = "objectexplorer", MenuOrder = 1, KeyGesture ="F8")]
    public sealed class ViewObjectExplorerCommand: SimpleCommand
    {
        public override void Execute(object parameter)
        {
            ObjectExplorerViewModel view = Workbench.Self.FindTool<ObjectExplorerViewModel>();
            if (view != null)
            {
                view.IsVisible = true;
                view.IsActive = true;
            }
        }
    }
}
