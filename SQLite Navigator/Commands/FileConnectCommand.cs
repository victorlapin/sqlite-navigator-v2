﻿using System;
using System.Data;
using SQLiteNavigator.Classes;
using SQLiteNavigator.Metadata;
using Microsoft.Win32;
using System.IO;

namespace SQLiteNavigator.Commands
{
    [ExportMainMenuCommand(Header="Connect To Database", Menu="File", MenuIcon="connect", MenuOrder=1, MenuCategory="Connect")]
    [ExportToolbarCommand(ToolbarIcon="connect", ToolTip="Connect To Database", ToolbarCategory="Connect", ToolbarText="Connect", ToolbarOrder=1)]
    public sealed class FileConnectCommand: SimpleCommand
    {
        public override void Execute(object parameter)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Database Files (*.db;*.db3;*.sqlite)|*.db;*.db3;*.sqlite|SQLite Navigator Solution Files (*.snsln)|*.snsln|All Files|*.*";
            if (dialog.ShowDialog(MainWindow.Instance).Value)
            {
                Workbench.Self.Solution.Connect(dialog.FileName, Path.GetExtension(dialog.FileName).ToLowerInvariant().Equals(".snsln"));
            }
        }
    }
}