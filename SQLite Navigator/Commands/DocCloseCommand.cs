﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLiteNavigator.ViewModels;
using SQLiteNavigator.Classes;

namespace SQLiteNavigator.Commands
{
    public sealed class DocCloseCommand : OwnerCommand
    {
        public DocCloseCommand(object owner)
            : base(owner)
        {

        }

        public override void Execute(object parameter)
        {
            Workbench.Self.Documents.Remove((DocViewModel)Owner);
        }
    }
}
