﻿using SQLiteNavigator.Metadata;

namespace SQLiteNavigator.Commands
{
    [ExportMainMenuCommand(Header = "Exit", Menu = "File", MenuOrder = 999, MenuCategory = "Exit")]
    public sealed class FileExitCommand : SimpleCommand
    {
        public override void Execute(object parameter)
        {
            MainWindow.Instance.Close();
        }
    }
}
