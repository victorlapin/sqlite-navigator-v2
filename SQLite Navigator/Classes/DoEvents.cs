﻿using System;
using System.Windows.Threading;

namespace SQLiteNavigator.Classes
{
    /// <summary>
    /// Class that implements operation similar to Application.DoEvents in Windows Forms
    /// </summary>
    public static class DoEvents
    {
        public static void Push()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background, new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }
        
        private static object ExitFrame(object arg)
        {
            (arg as DispatcherFrame).Continue = false;
            return null;
        }
    }
}
