﻿
namespace SQLiteNavigator.Classes
{
    public enum RecentSessionAge { Today, Yesterday, LastWeek, TwoWeeksAgo, ThreeWeeksAgo, FourWeeksAgo, Older }
    public enum DisconnectType { Silent, Ask }
}
