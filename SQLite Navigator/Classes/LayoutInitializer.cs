﻿using System;
using Xceed.Wpf.AvalonDock.Layout;
using SQLiteNavigator.ViewModels;
using System.Linq;
using SQLiteNavigator.Metadata;
using System.Windows;

namespace SQLiteNavigator.Classes
{
    class LayoutInitializer: ILayoutUpdateStrategy
    {
        #region ILayoutUpdateStrategy Members

        public void AfterInsertAnchorable(LayoutRoot layout, LayoutAnchorable anchorableShown)
        {
            
        }

        public void AfterInsertDocument(LayoutRoot layout, LayoutDocument anchorableShown)
        {
            
        }

        public bool BeforeInsertAnchorable(LayoutRoot layout, LayoutAnchorable anchorableToShow, ILayoutContainer destinationContainer)
        {
            if (anchorableToShow.Root != null)
            {
                anchorableToShow.Dock();
                return true;
            }

            LayoutAnchorablePane destPane = destinationContainer as LayoutAnchorablePane;
            if (destinationContainer != null &&
                destinationContainer.FindParent<LayoutFloatingWindow>() != null)
                return false;

            IDockContentMetadata metadata = (IDockContentMetadata)(anchorableToShow.Content as PaneViewModel).Tag;
            if (metadata != null)
            {
                if (metadata.Anchor != AnchorableShowStrategy.Most)
                {
                    anchorableToShow.AddToLayout(layout.Manager, metadata.Anchor);
                    (anchorableToShow.Parent as LayoutAnchorablePane).DockWidth = new GridLength(250);
                    (anchorableToShow.Parent as LayoutAnchorablePane).DockHeight = new GridLength(150);
                }
                return true;
            }
            return false;
        }

        public bool BeforeInsertDocument(LayoutRoot layout, LayoutDocument anchorableToShow, ILayoutContainer destinationContainer)
        {
            if (anchorableToShow.Root != null)
            {
                anchorableToShow.DockAsDocument();
                return true;
            }

            (destinationContainer as LayoutDocumentPane).Children.Add(anchorableToShow);
            return true;
        }

        #endregion
    }
}
