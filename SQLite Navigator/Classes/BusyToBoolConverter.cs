﻿using System;
using System.Windows.Data;

namespace SQLiteNavigator.Classes
{
    public class BusyToBoolConverter: IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool arg;
            try { arg = (bool)value[0]; }
            catch { arg = false; }
            return !arg;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new object[] { Binding.DoNothing };
        }
    }
}
