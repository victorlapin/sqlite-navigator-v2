﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using SQLiteNavigator.Classes.Nodes;
using SQLiteNavigator.Helpers;

namespace SQLiteNavigator.Classes.Info
{
    internal class SQLiteCheckInfo: IBaseInfo
    {
        private string name;
        [DisplayName("Constraint Name")]
        [Category("Constraint")]
        public string Name
        {
            get { return name; }
        }

        private string type;
        [DisplayName("Constraint Type")]
        [Category("Constraint")]
        public string Type
        {
            get { return type; }
        }

        private string catalog;
        [DisplayName("Constraint Catalog")]
        [Category("Constraint")]
        public string Catalog
        {
            get { return catalog; }
        }

        private string table;
        [DisplayName("Constraint Table")]
        [Category("Constraint")]
        public string Table
        {
            get { return table; }
        }

        private string expression;
        [DisplayName("Expression")]
        [Category("Constraint")]
        public string Expression
        {
            get { return expression; }
        }

        #region IBaseInfo Members

        public void FillInfo(TreeNode argument)
        {
            //заполняем поля
            name = argument.Text.ToString();
            type = "CHECK";
            TreeNode tableNode = TreeHelper.GetTable(argument);            
            table = tableNode.Text.ToString();
            catalog = TreeHelper.GetCatalog(tableNode).Text.ToString();
            this.expression = argument.Tag.ToString();
        }

        public IBaseInfo Clone()
        {
            return new SQLiteCheckInfo();
        }

        #endregion
    }
}
