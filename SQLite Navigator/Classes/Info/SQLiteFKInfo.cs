﻿using System.Data.SQLite;
using System.ComponentModel;
using System.Data;
using System;
using SQLiteNavigator.Classes.Nodes;
using SQLiteNavigator.Metadata;

namespace SQLiteNavigator.Classes.Info
{
    [ExportPropertiesInfo(Contract = "ForeignKeyTreeNode")]
    internal class SQLiteFKInfo: IBaseInfo
    {
        private string name;
        [DisplayName("Constraint Name")]
        [Category("Constraint")]
        public string Name
        {
            get { return name; }
        }

        private string type;
        [DisplayName("Constraint Type")]
        [Category("Constraint")]
        public string Type
        {
            get { return type; }
        }

        private string catalog;
        [DisplayName("Constraint Catalog")]
        [Category("Constraint")]
        public string Catalog
        {
            get { return catalog; }
        }

        private bool isDeferrable;
        [DisplayName("Is Deferrable")]
        [Category("Constraint")]
        public bool IsDeferrable
        {
            get { return isDeferrable; }
        }

        private bool initiallyDeferred;
        [DisplayName("Initially Deferred")]
        [Category("Constraint")]
        public bool InitiallyDeferred
        {
            get { return initiallyDeferred; }
        }

        private string table;
        [DisplayName("Table Name")]
        [Category("Constraint Table")]
        public string Table
        {
            get { return table; }
        }

        private string tableCatalog;
        [DisplayName("Table Catalog")]
        [Category("Constraint Table")]
        public string TableCatalog
        {
            get { return tableCatalog; }
        }

        private string columns;
        [DisplayName("Table Columns")]
        [Category("Constraint Table")]
        public string Columns
        {
            get { return columns; }
        }

        private string refTable;
        [DisplayName("Reference Name")]
        [Category("Reference Table")]
        public string RefTable
        {
            get { return refTable; }
        }

        private string refTableCatalog;
        [DisplayName("Reference Catalog")]
        [Category("Reference Table")]
        public string RefTableCatalog
        {
            get { return refTableCatalog; }
        }

        private string refColumns;
        [DisplayName("Reference Columns")]
        [Category("Reference Table")]
        public string RefColumns
        {
            get { return refColumns; }
        }

        #region IBaseInfo Members

        public void FillInfo(TreeNode argument)
        {
            DataRow row = (DataRow)argument.Tag;
            if (row == null) return;

            name = argument.Text.ToString();
            type = row["constraint_type"].ToString();
            catalog = row["constraint_catalog"].ToString();
            isDeferrable = (bool)row["is_deferrable"];
            initiallyDeferred = (bool)row["initially_deferred"];
            table = row["table_name"].ToString();
            tableCatalog = row["table_catalog"].ToString();
            columns = row["fkey_from_column"].ToString();
            refTable = row["fkey_to_table"].ToString();
            refTableCatalog = row["fkey_to_catalog"].ToString();
            refColumns = row["fkey_to_column"].ToString();
        }

        public IBaseInfo Clone()
        {
            return new SQLiteFKInfo();
        }

        #endregion
    }
}
