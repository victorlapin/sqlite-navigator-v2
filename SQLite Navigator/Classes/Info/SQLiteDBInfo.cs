﻿using System;
using System.Data.SQLite;
using System.ComponentModel;
using SQLiteNavigator.Metadata;
using SQLiteNavigator.Classes.Nodes;

namespace SQLiteNavigator.Classes.Info
{
    [ExportPropertiesInfo(Contract = "DatabaseTreeNode")]
    internal class SQLiteDBInfo: IBaseInfo
    {
        private string name;
        [DisplayName("Alias")]
        [Category("Database")]
        [Description("Alias of the connection.")]
        public string Name
        {
            get { return name; }
        }

        private string fullPath;
        [DisplayName("Location")]
        [Category("Database")]
        [Description("Location of the database file.")]
        public string FullPath
        {
            get { return fullPath; }
        }

        private bool isEncrypted;
        [DisplayName("Encrypted")]
        [Category("Database")]
        [Description("Whether connection is encrypted or not.")]
        public bool IsEncrypted
        {
            get { return isEncrypted; }
        }

        #region IBaseInfo Members

        public void FillInfo(TreeNode argument)
        {
            SQLiteConnectionStringBuilder builder = new SQLiteConnectionStringBuilder(Workbench.Self.Solution.Connection.ConnectionString);
            name = argument.Tag.ToString();
            fullPath = builder.DataSource;
            isEncrypted = (!builder.Password.Equals(String.Empty));            
        }

        public IBaseInfo Clone()
        {
            return new SQLiteDBInfo();
        }

        #endregion
    }
}
