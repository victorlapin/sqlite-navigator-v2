﻿using System.ComponentModel;
using SQLiteNavigator.Metadata;
using SQLiteNavigator.Classes.Nodes;

namespace SQLiteNavigator.Classes.Info
{
    [ExportPropertiesInfo(Contract="SolutionTreeNode")]
    internal class SQLiteSolutionInfo: IBaseInfo
    {
        private string name;
        [DisplayName("Name")]
        [Category("Solution")]
        [Description("Name of the solution.")]
        public string Name
        {
            get { return name; }
        }

        private string fileName;
        [DisplayName("File Name")]
        [Category("Solution")]
        [Description("Name of the solution file.")]
        public string FileName
        {
            get { return fileName; }
        }

        private string location;
        [DisplayName("Location")]
        [Category("Solution")]
        [Description("Location of the solution file.")]
        public string Location
        {
            get { return location; }
        }

        private string format;
        [DisplayName("Format Version")]
        [Category("Solution")]
        [Description("Solution file format version.")]
        public string Format
        {
            get { return format; }
        }

        #region IBaseInfo Members

        public void FillInfo(TreeNode argument)
        {
            Solution solution = (Solution)argument.Tag;
            if (solution == null) return;
            
            name = solution.Name;
            fileName = solution.FileName;
            location = solution.Directory;
            format = solution.FormatVersion;
        }

        public IBaseInfo Clone()
        {
            return new SQLiteSolutionInfo();
        }

        #endregion
    }
}
