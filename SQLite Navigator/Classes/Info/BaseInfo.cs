﻿using SQLiteNavigator.Classes.Nodes;

namespace SQLiteNavigator.Classes.Info
{
    public interface IBaseInfo
    {
        void FillInfo(TreeNode argument);

        IBaseInfo Clone();
    }
}
