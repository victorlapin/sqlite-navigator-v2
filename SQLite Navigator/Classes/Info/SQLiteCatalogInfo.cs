﻿using System.ComponentModel;
using SQLiteNavigator.Metadata;
using SQLiteNavigator.Classes.Nodes;
using System.Data;
using System.Data.SQLite;
using System;

namespace SQLiteNavigator.Classes.Info
{
    [ExportPropertiesInfo(Contract = "CatalogTreeNode")]
    internal class SQLiteCatalogInfo: IBaseInfo
    {
        private string name;
        [DisplayName("Alias")]
        [Category("Catalog")]
        [Description("Alias of the catalog.")]
        public string Name
        {
            get { return name; }
        }

        private bool attached;
        [DisplayName("Attached")]
        [Category("Catalog")]
        [Description("Specifies whether selected catalog is attached database.")]
        public bool Attached
        {
            get { return attached; }
        }

        private string path;
        [DisplayName("Location")]
        [Category("Catalog")]
        [Description("Location of the file.")]
        public string Path
        {
            get { return path; }
        }

        #region IBaseInfo Members

        public void FillInfo(TreeNode argument)
        {
            DataRow row = (DataRow)argument.Tag;
            if (row == null) return;

            name = argument.Text.ToString();
            attached = !(name.Equals("main") || name.Equals("temp"));
            path = attached ? row["CATALOG_NAME"].ToString() : String.Empty;
        }

        public IBaseInfo Clone()
        {
            return new SQLiteCatalogInfo();
        }

        #endregion
    }
}
