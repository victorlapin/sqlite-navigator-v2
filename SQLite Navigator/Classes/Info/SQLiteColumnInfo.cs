﻿using System.ComponentModel;
using System;
using System.Data.SQLite;
using System.Data;
using SQLiteNavigator.Classes.Nodes;
using SQLiteNavigator.Metadata;

namespace SQLiteNavigator.Classes.Info
{
    [ExportPropertiesInfo(Contract = "ColumnTreeNode")]
    internal class SQLiteColumnInfo: IBaseInfo
    {
        private string name;
        [DisplayName("(Name)")]
        [Category("Column")]
        [Description("The column's name.")]
        public string Name
        {
            get { return name; }
        }

        private string dataType;
        [DisplayName("Data Type")]
        [Category("Column")]
        [Description("Exposes the data type name for the referenced column.")]
        public string DataType
        {
            get { return dataType; }
        }

        private string systemType;
        [DisplayName("System Type")]
        [Category("Column")]
        [Description("System-supplied data type.")]
        public string SystemType
        {
            get { return systemType; }
        }

        private string size;
        [DisplayName("Size")]
        [Category("Column")]
        [DefaultValue("0")]
        [Description("Specifies the maximum number of characters or bytes accepted by the referenced column.")]
        public string Size
        {
            get { return size; }
        }

        private string defaultValue;
        [DisplayName("Default value")]
        [Category("Column")]
        [Description("Specifies default value for a column.")]
        public string DefaultValue
        {
            get { return defaultValue; }
        }

        private bool isPrimaryKey;
        [DisplayName("Primary Key")]
        [Category("Column")]
        [DefaultValue(false)]
        [Description("Specifies whether referenced column is a primary key.")]
        public bool IsPrimaryKey
        {
            get { return isPrimaryKey; }
        }

        private bool allowNulls;
        [DisplayName("Allow nulls")]
        [Category("Column")]
        [DefaultValue(true)]
        [Description("Specifies whether a column may have a NULL value.")]
        public bool AllowNulls
        {
            get { return allowNulls; }
        }

        private bool isUnique;
        [DisplayName("Unique")]
        [Category("Column")]
        [DefaultValue(false)]
        [Description("Specifies whether a column's value must be unique.")]
        public bool IsUnique
        {
            get { return isUnique; }
        }

        private bool isAutoincrement;
        [DisplayName("Autoincrement")]
        [Category("Column")]
        [DefaultValue(false)]
        [Description("Specifies whether a column's value incrementing automatically.")]
        public bool IsAutoincrement
        {
            get { return isAutoincrement; }
        }

        private string collate;
        [DisplayName("Collate")]
        [Category("Column")]
        [Description("Shows the column-level collation.")]
        [DefaultValue("binary")]
        public string Collate
        {
            get { return collate; }
        }

        #region IBaseInfo Members

        public void FillInfo(TreeNode argument)
        {
            DataRow row = (DataRow)argument.Tag;
            if (row == null) return;

            name = row["column_name"].ToString();
            isPrimaryKey = (bool)row["primary_key"];
            isUnique = ((bool)row["unique"]) && (!(bool)row["primary_key"]);
            allowNulls = (bool)row["is_nullable"];
            isAutoincrement = (bool)row["autoincrement"];
            collate = row["collation_name"].ToString().ToLowerInvariant();
            dataType = row["data_type"].ToString().ToLowerInvariant();
            if (dataType.Contains("text") || dataType.Contains("varchar"))
                size = row["character_maximum_length"].ToString();
            defaultValue = row["column_default"].ToString();

            DataRow[] rows = Workbench.Self.Solution.Connection.GetSchema("DataTypes").Select(String.Format("TypeName = '{0}'", dataType));
            if (rows.Length > 0)
                systemType = rows[0].ItemArray[5].ToString().
                    Replace("System.", String.Empty);
        }

        public IBaseInfo Clone()
        {
            return new SQLiteColumnInfo();
        }

        #endregion
    }
}
