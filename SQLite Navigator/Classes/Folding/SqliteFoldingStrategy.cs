﻿using System;
using System.Collections.Generic;
using ICSharpCode.AvalonEdit.Folding;
using ICSharpCode.AvalonEdit.Document;

namespace SQLiteNavigator.Classes.Folding
{
    class SqliteFoldingStrategy: AbstractFoldingStrategy
    {
        private Stack<TextLocation> stack;
        private Stack<TextLocation> comments;
        private string[] beginKeywords = new string[]
        {
            "ALTER", "ATTACH", "CREATE", "DETACH", "DROP", "INSERT", "SELECT", "UPDATE", "DELETE", "BEGIN"
        };
        string[] endKeywords = new string[]
        {
            ";", "END"
        };

        private bool IsBeginKeyword(string word)
        {
            bool result = false;
            for (int i = 0; i < beginKeywords.Length; i++)
            {
                if (word.ToUpperInvariant().Contains(beginKeywords[i]))
                    result = true;
            }
            return result;
        }

        private bool IsEndKeyword(string word)
        {
            bool result = false;
            for (int i = 0; i < endKeywords.Length; i++)
            {
                if (word.ToUpperInvariant().Contains(endKeywords[i]))
                    result = true;
            }
            return result;
        }

        private bool IsBeginComment(string word)
        {
            return word.Contains("/*");
        }

        private bool IsEndComment(string word)
        {
            return word.Contains("*/");
        }

        private bool IsInsideComment(TextDocument document, int offset)
        {
            bool result = false;
            if (comments.Count > 0)
                result = (document.GetOffset(comments.Peek()) < offset);
            return result;
        }

        public SqliteFoldingStrategy()
        {
            stack = new Stack<TextLocation>();
            comments = new Stack<TextLocation>();
        }

        public override IEnumerable<NewFolding> CreateNewFoldings(TextDocument document, out int firstErrorOffset)
        {
            List<NewFolding> result = new List<NewFolding>();

            if (document.TextLength > 0)
            {
                foreach (DocumentLine line in document.Lines)
                {
                    string text = document.GetText(line);
                    if (IsBeginComment(text))
                        comments.Push(new TextLocation(line.LineNumber, 0));
                    if (IsBeginKeyword(text) && !IsInsideComment(document, line.Offset) && !text.StartsWith("--"))
                        stack.Push(new TextLocation(line.LineNumber, 0));

                    if (IsEndComment(text) || (line.NextLine == null && comments.Count > 0))
                    {
                        if (comments.Count > 0)
                        {
                            TextLocation start = comments.Pop();
                            if (start.Line != line.LineNumber)
                                result.Add(new NewFolding(document.GetOffset(start), line.Offset + line.Length));                            
                        }
                    }
                    
                    if ((IsEndKeyword(text) && !IsInsideComment(document, line.Offset) && !text.StartsWith("--")) || line.NextLine == null)
                    {
                        if (stack.Count > 0)
                        {
                            TextLocation start = stack.Pop();
                            if (start.Line != line.LineNumber)
                                result.Add(new NewFolding(document.GetOffset(start), line.Offset + line.Length));
                        }
                    }
                }
            }

            firstErrorOffset = -1;
            result.Sort(new SqliteFoldingComparer());
            return result;
        }
    }
}
