﻿using System.Collections.Generic;
using ICSharpCode.AvalonEdit.Folding;

namespace SQLiteNavigator.Classes.Folding
{
    public class SqliteFoldingComparer: IComparer<NewFolding>
    {
        public int Compare(NewFolding x, NewFolding y)
        {
            return x.StartOffset.CompareTo(y.StartOffset);
        }
    }
}
