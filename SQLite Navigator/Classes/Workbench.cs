﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using SQLiteNavigator.ViewModels;
using System.Linq;

namespace SQLiteNavigator.Classes
{
    /// <summary>
    /// Main class that contains all data for working process
    /// </summary>
    public class Workbench: ViewModelBase
    {
        static Workbench _self = new Workbench();
        public static Workbench Self { get { return _self; } }

        private ObservableCollection<OutputEntry> output = new ObservableCollection<OutputEntry>();
        public ReadOnlyObservableCollection<OutputEntry> Output { get; private set; }
        public Solution Solution { get; private set; }
        public ObservableCollection<RecentSession> RecentSessions { get; private set; }

        public ObservableCollection<ToolViewModel> Tools { get; private set; }
        public ObservableCollection<PaneViewModel> Documents { get; private set; }

        private NotifyCollectionChangedEventHandler OnOutputChanged;
        public event NotifyCollectionChangedEventHandler OutputChanged
        {
            add { OnOutputChanged += value; }
            remove { OnOutputChanged -= value; }
        }

        protected Workbench()
        {
            Output = new ReadOnlyObservableCollection<OutputEntry>(output);
            Solution = new Solution();
            RecentSessions = new ObservableCollection<RecentSession>();
            RecentSessions.CollectionChanged += delegate(object sender, NotifyCollectionChangedEventArgs e)
            {
                if (e.Action == NotifyCollectionChangedAction.Add)
                {
                    foreach (object s in e.NewItems)
                        (s as RecentSession).SetSessionAge();
                }
            };
            output.CollectionChanged += delegate(object sender, NotifyCollectionChangedEventArgs e)
            {
                if (OnOutputChanged != null)
                    OnOutputChanged(sender, e);
            };

            Tools = new ObservableCollection<ToolViewModel>();
            Documents = new ObservableCollection<PaneViewModel>();
        }

        #region Logger methods
        
        public void WriteOutput(string GlyphSource, string Text)
        {
            _self.output.Add(new OutputEntry(GlyphSource, Text));
            DoEvents.Push();
        }

        public void WriteError(string Text)
        {
            _self.output.Add(new OutputEntry("error", Text, true));
            DoEvents.Push();
        }

        public void WriteError(string Text, Exception ex)
        {
            _self.WriteError(String.Format("{0}: {1}", Text, ex.Message));
        }
        
	#endregion        

        #region Recent sessions methods

        public void AddRecentSession(string path, bool IsSolution)
        {
            bool found = false;
            //if we already have this session in a collection, just update it
            foreach (RecentSession session in RecentSessions)
            {
                if (Path.Combine(session.Path, session.Name).Equals(path))
                {
                    session.LastAccessed = DateTime.Today;
                    session.SetSessionAge();
                    found = true;
                }
            }

            //if not, create new
            if (!found)
            {
                RecentSession s = new RecentSession
                {
                    Name = Path.GetFileName(path),
                    Path = Path.GetDirectoryName(path),
                    IsSolution = IsSolution,
                    LastAccessed = DateTime.Today
                };
                _self.RecentSessions.Add(s);
                WriteOutput("clock", String.Format("{0} added to recent connections", s.Name));
            }
        }

        #endregion

        #region ActiveDocument

        private DocViewModel _activeDocument = null;
        public DocViewModel ActiveDocument
        {
            get { return _activeDocument; }
            set
            {
                if (_activeDocument != value)
                {
                    _activeDocument = value;
                    RaisePropertyChanged("ActiveDocument");
                    if (ActiveDocumentChanged != null)
                        ActiveDocumentChanged(this, EventArgs.Empty);
                }
            }
        }

        public event EventHandler ActiveDocumentChanged;

        #endregion

        public T FindTool<T>() where T : ToolViewModel
        {
            return (T)Tools.Where(t => t.GetType().Equals(typeof(T))).FirstOrDefault();
        }

        public T FindDocument<T>() where T : DocViewModel
        {
            return (T)Documents.Where(t => t.GetType().Equals(typeof(T))).FirstOrDefault();
        }
    }
}
