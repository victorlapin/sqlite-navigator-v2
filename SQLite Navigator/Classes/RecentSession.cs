﻿using System;

namespace SQLiteNavigator.Classes
{
    public sealed class RecentSession
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public DateTime LastAccessed { get; set; }
        public bool IsSolution { get; set; }
        public RecentSessionAge Age { get; set; }
        
        public static RecentSessionAge CalculateAge(DateTime dt)
        {
            int days = (DateTime.Today.Date - dt.Date).Days;
            if (days == 0)
                return RecentSessionAge.Today;
            else if (days == 1)
                return RecentSessionAge.Yesterday;
            else if (days >= 2 && days <=7)
                return RecentSessionAge.LastWeek;
            else if (days >= 8 && days <=14)
                return RecentSessionAge.TwoWeeksAgo;
            else if (days >= 15 && days <=21)
                return RecentSessionAge.ThreeWeeksAgo;
            else if (days >= 22 && days <=28)
                return RecentSessionAge.FourWeeksAgo;
            else return RecentSessionAge.Older;
        }
        
        public void SetSessionAge()
        {
            this.Age = RecentSession.CalculateAge(this.LastAccessed);
        }
    }
}
