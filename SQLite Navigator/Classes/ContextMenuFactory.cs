﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using SQLiteNavigator.Metadata;

namespace SQLiteNavigator.Classes
{
    static class ContextMenuFactory
    {
        private static List<Lazy<ICommand, IContextMenuCommandMetadata>> _source;

        public static void SetSource(Lazy<ICommand, IContextMenuCommandMetadata>[] source)
        {
            _source = source.ToList();
        }

        public static IEnumerable<Lazy<ICommand, IContextMenuCommandMetadata>> GetItems()
        {
            return _source;
        }
    }
}
