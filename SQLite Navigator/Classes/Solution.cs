﻿using System;
using System.IO;
using System.Data;
using System.Data.SQLite;
using System.Text;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Windows;
using System.Xml;
using SQLiteNavigator.Windows;
using System.Linq;
using SQLiteNavigator.Classes.Helpers;

namespace SQLiteNavigator.Classes
{
    /// <summary>
    /// Class for solution routines
    /// </summary>
    public sealed class Solution
    {
        public string Name { get; private set; }
        public string FileName { get; set; }
        public string Directory { get; private set; }
        public string ExplorerDbName { get; set; }
        public XDocument Body { get; private set; }
        public static string FormatVersionActual { get { return "2.00"; } }
        public Dictionary<string, string> Catalogs { get; private set; }
        public string FormatVersion { get; private set; }
        public bool Modified { get; private set; }
        public SQLiteConnection Connection { get; private set; }
        public static string RecoveryFileName { get { return "AutoRecovery.snsln"; } }
        public List<string> Queries { get; private set; }

        public Solution()
        {
            Connection = new SQLiteConnection();
            Connection.ParseViaFramework = true;
            Reset();
        }

        public void Reset()
        {
            Name = "New Solution";
            Directory = String.Empty;
            ExplorerDbName = String.Empty;
            Body = null;
            FileName = String.Empty;
            Catalogs = new Dictionary<string,string>();
            FormatVersion = FormatVersionActual;
            Modified = false;
            Queries = new List<string>();
        }

        private void FillFromPath(string path)
        {
            Name = Path.GetFileNameWithoutExtension(path);
            FileName = Path.GetFileName(path);
            Directory = Path.GetDirectoryName(path);
        }

        public void AddCatalog(string alias, string password)
        {
            byte[] cypher = new byte[password.Length];
            for (int i = 0; i < password.Length; i++)
                cypher[i] = Convert.ToByte(password[i]);
            Catalogs.Add(alias, Convert.ToBase64String(cypher));
        }

        public void DeleteCatalog(string alias)
        {
            Catalogs.Remove(alias);
        }

        #region Save/Load

        public XDocument GenerateSolution()
        {
            if (Connection.State == ConnectionState.Open)
            {
                EnumerableRowCollection<DataRow> catalogCollection = Connection.GetSchema(SQLiteMetaDataCollectionNames.Catalogs).AsEnumerable();

                XDocument xml = new XDocument(
                    new XDeclaration("1.0", Encoding.UTF8.HeaderName, "yes"),
                    new XComment("SQLite Navigator Solution File"),
                    new XElement("Solution",
                                 new XAttribute("FormatVersion", FormatVersionActual),
                                 new XElement("Database",
                                              catalogCollection
                                              .Where(r => r[0].ToString().ToLowerInvariant().Equals("main"))
                                              .Select(r => new XAttribute("Location", new Uri(r[1].ToString()).ToString()))
                                             ),
                                 new XElement("AttachedDatabases",
                                              catalogCollection
                                              .Where(r => (!r[0].ToString().ToLowerInvariant().Equals("main") && !r[0].ToString().ToLowerInvariant().Equals("temp")))
                                              .Select(r => new XElement("AttachedDatabase",
                                                                        new XAttribute("Alias", r[0].ToString()),
                                                                        new XAttribute("Location", new Uri(r[1].ToString()).ToString()),
                                                                        new XAttribute("Password", Catalogs[r[0].ToString()])
                                                                       )
                                                     )
                                             ),
                                new XElement("Queries",
                                            Queries.Select(q => new XElement("Query",
                                                                                        new XAttribute("Location", q)
                                                                                        )
                                                    )
                                            ),
                                new XElement("Files",
                                            Queries.Select(f => new XElement("File",
                                                                                        new XAttribute("Location", f)
                                                                                        )
                                                    )
                                            )
                                )
                   );

                return xml;
            }
            else
                return null;
        }

        public void SaveSolution()
        {
            Body = GenerateSolution();
            if (Body != null && !FileName.Equals(String.Empty))
            {
                Body.Save(Path.Combine(Directory, FileName));
            }
        }

        public void LoadSolution(string path)
        {
            bool isRecovery = path.Equals(Path.Combine(Path.GetTempPath(), Solution.RecoveryFileName));
            Workbench.Self.WriteOutput("solution", (isRecovery) ? "Loading recovery solution..." : String.Format("Loading solution {0}...", path));
            XDocument xml = XDocument.Load(path);
            bool errorFlag = false;

            this.FillFromPath(path);
            this.FormatVersion = xml.Element("Solution").Attribute("FormatVersion").Value;
            this.Body = xml;
          
            try
            {
                XElement db = xml.Element("Solution").Element("Database");
                string uri = new Uri(db.Attribute("Location").Value).LocalPath;
                if (!File.Exists(uri))
                    throw new FileNotFoundException(String.Format("Could not find database {0}", uri));
                this.Connect(uri);
                IEnumerable<XElement> attached = xml.Element("Solution").Element("AttachedDatabases").Elements("AttachedDatabase");
                foreach (XElement catalog in attached)
                {
                    byte[] cypher = Convert.FromBase64String(catalog.Attribute("Password").Value);
                    StringBuilder password = new StringBuilder();
                    foreach (byte b in cypher)
                        password.Append(Convert.ToChar(b));
                    if (!Attach(new Uri(catalog.Attribute("Location").Value).LocalPath, catalog.Attribute("Alias").Value, password.ToString()))
                        errorFlag = true;
                }

                IEnumerable<XElement> dummy;
                if (xml.Element("Solution").Element("Queries") != null)
                {
                    dummy = xml.Element("Solution").Element("Queries").Elements("Query");
                    foreach (XElement d in dummy)
                    {
                        this.Queries.Add(d.Attribute("Location").Value);
                    }
                }
            }
            catch (XmlException ex)
            {
                Workbench.Self.WriteError("Invalid solution file format", ex);
            }

            if (errorFlag)
                Workbench.Self.WriteOutput("warning", "Solution loaded with errors");
            else
                Workbench.Self.WriteOutput("solution", "Solution successfully loaded");
        }

        #endregion

        #region Connect/Disconnect

        internal bool Disconnect(DisconnectType dt = DisconnectType.Ask)
        {
            if (Connection.State == ConnectionState.Open)
            {
                MessageBoxResult mr;
                if (dt == DisconnectType.Silent)
                    mr = MessageBoxResult.Yes;
                else
                    mr = MessageBox.Show(MainWindow.Instance, "Current session will be closed. Are you sure?", SettingsHelper.ApplicationName,
                                         MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (mr == MessageBoxResult.No)
                    return false;
                else
                {
                    Workbench.Self.WriteOutput("disconnect", "Closing session...");
                    Connection.Close();
                    this.Reset();
                    MainWindow.Instance.Title = SettingsHelper.ApplicationName;
                    return true;
                }
            }
            else
                return true;
        }

        internal void Connect(string Dbname, bool IsSolution = false, bool needsAuth = true)
        {
            if (!Disconnect()) return;
            try
            {
                if (IsSolution)
                {
                    //loading solution
                    this.LoadSolution(Dbname);
                }
                else
                {
                    //connecting to standalone database
                    SQLiteConnectionStringBuilder builder = new SQLiteConnectionStringBuilder();
                    if (needsAuth)
                    {
                        PasswordWindow window = new PasswordWindow();
                        window.Owner = MainWindow.Instance;
                        window.lblDatabase.Text = "Database Name:";
                        window.lblPassword.Text = "Database Password:";
                        window.edtDatabase.IsReadOnly = true;
                        window.edtDatabase.IsReadOnlyCaretVisible = false;
                        window.edtDatabase.Background = SystemColors.ControlBrush;
                        window.edtDatabase.Text = Path.GetFileName(Dbname);
                        if (!window.ShowDialog().Value)
                            return;
                        if (!String.IsNullOrEmpty(window.edtPassword.Password))
                            builder.Password = window.edtPassword.Password;
                    }

                    builder.DataSource = Dbname;
                    builder.PageSize = 4096;
                    builder.ForeignKeys = true;
                    builder.FailIfMissing = true;
                    this.Connection.ConnectionString = builder.ConnectionString;

                    this.ExplorerDbName = (Dbname.Equals(":memory:"))
                        ? "In-memory Database" : Path.GetFileNameWithoutExtension(Dbname);

                    if (File.Exists(Dbname))
                    {
                        Workbench.Self.WriteOutput("connect", String.Format("Connecting to {0}...", Dbname));
                        this.Connection.Open();
                        this.AddCatalog("main", builder.Password);
                    }
                    else
                    {
                        if (Dbname.Equals(":memory:"))
                            Workbench.Self.WriteOutput("database_create", "Creating in-memory database...");
                        else
                            Workbench.Self.WriteOutput("database_create", String.Format("Creating {0}...", Dbname));
                        this.Connection.Open();
                    }
                }

                //if we're creating an in-memory database, there's no need to add it to recent sessions
                if (!Dbname.Equals(":memory:"))
                {
                    Workbench.Self.AddRecentSession(Dbname, IsSolution);
                }
                MainWindow.Instance.Title = String.Format("{0} - {1}", (IsSolution) ? this.Name : this.ExplorerDbName, SettingsHelper.ApplicationName);
            }
            catch (Exception ex)
            {
                Workbench.Self.WriteError((IsSolution) ? "Failed to load solution" : "Could not connect to database", ex);
            }
        }

        #endregion

        #region Attach/Detach
        
        public void AttachDialog(string fname)
        {
            AttachDialog(fname, String.Empty);
        }

        private void AttachDialog(string fname, string alias)
        {
            string dbname = String.Empty;
            string password = String.Empty;
            
            PasswordWindow window = new PasswordWindow();
            window.Owner = MainWindow.Instance;
            window.lblDatabase.Text = "Database Alias:";
            window.edtDatabase.Text = String.IsNullOrEmpty(alias) ? Path.GetFileNameWithoutExtension(fname) : alias;

            if (!window.ShowDialog().Value)
                return;
            dbname = (String.IsNullOrEmpty(window.edtDatabase.Text)) 
                ? Path.GetFileNameWithoutExtension(fname) : window.edtDatabase.Text;
            password = window.edtPassword.Password;

            Attach(fname, dbname, password);
        }

        public bool Attach(string fname, string dbname, string password)
        {
            using (SQLiteCommand command = new SQLiteCommand(Workbench.Self.Solution.Connection))
            {
                command.CommandText = (password.Equals(String.Empty))
                    ? String.Format(@"attach database ""{0}"" as {1}", fname, dbname)
                    : String.Format(@"attach database ""{0}"" as {1} key '{2}'", fname, dbname, password);
                try
                {
                    Workbench.Self.WriteOutput("catalog_attached", String.Format("Attaching {0}...", dbname));
                    command.ExecuteNonQuery();
                    Workbench.Self.WriteOutput("ok", "Attached");
                    Workbench.Self.Solution.AddCatalog(dbname, password);
                    Workbench.Self.Solution.Modified = true;
                    return true;
                }
                catch (Exception ex)
                {
                    Workbench.Self.WriteError("Attach failed", ex);
                    return false;
                }
            }
        }
        
        public void Detach(string dbname)
        {
            if (MessageBox.Show(String.Format("Are you sure to detach {0}?", dbname), SettingsHelper.ApplicationName,
                                MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                using (SQLiteCommand command = new SQLiteCommand(Workbench.Self.Solution.Connection))
                {
                    command.CommandText = String.Format("detach database {0}", dbname);
                    try
                    {
                        Workbench.Self.WriteOutput("catalog_attached", String.Format("Detaching {0}...", dbname));
                        command.ExecuteNonQuery();
                        Workbench.Self.WriteOutput("ok", "Detached");
                        Workbench.Self.Solution.DeleteCatalog(dbname);
                        Workbench.Self.Solution.Modified = true;
                    }
                    catch (Exception ex)
                    {
                        Workbench.Self.WriteError("Detach failed", ex);
                    }
                }
            }
        }

        #endregion
    }
}
