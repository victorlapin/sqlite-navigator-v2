﻿using ICSharpCode.AvalonEdit.CodeCompletion;
using System.Collections.Generic;
using ICSharpCode.AvalonEdit;
using System.Linq;
using SQLiteNavigator.Classes.Helpers;

namespace SQLiteNavigator.Classes.IntelliSense
{
    public class CompletionWindowResolver: ICompletionWindowResolver
    {
        private readonly string _text;
        private readonly int _position;
        private readonly string _input;
        private readonly TextEditor _target;


        private readonly List<ICompletionDataProvider> _dataProviders = new List<ICompletionDataProvider>();

        public CompletionWindowResolver(string text, int position, string input, TextEditor textEditor)
        {
            _text = text;
            _position = position;
            _input = input;
            _target = textEditor;

            _dataProviders.Add(new FileCompletionDataProvider());
            _dataProviders.Add(new InsightCompletionDataProvider());
            if (Workbench.Self.Solution.Connection.State == System.Data.ConnectionState.Open)
                _dataProviders.Add(new SchemaCompletionDataProvider());
        }

        public CompletionWindowBase Resolve()
        {

            var hiName = string.Empty;
            if (_target.SyntaxHighlighting != null)
            {
                hiName = _target.SyntaxHighlighting.Name;
            }

            var cdata = _dataProviders.SelectMany(x => x.GetData(_text, _position, _input, hiName)).ToList();
            int count = cdata.Count;
            if (count > 0)
            {
                cdata.Sort(new CompletionDataComparer());
                CompletionWindowBase completionWindow;

                //whether we should show completion or insight window
                if (_input.Equals("("))
                {
                    completionWindow = new InsightWindow(_target.TextArea);
                    completionWindow.Content = (cdata[0] as InsightCompletionData).Content;
                }
                else
                {
                    completionWindow = new CompletionWindow(_target.TextArea);
                    var data = (completionWindow as CompletionWindow).CompletionList.CompletionData;

                    foreach (var completionData in cdata)
                    {
                        data.Add(completionData);
                    }
                }

                completionWindow.Show();
                completionWindow.Closed += delegate
                {
                    completionWindow = null;
                };
                return completionWindow;
            }
            return null;
        }
    }
}
