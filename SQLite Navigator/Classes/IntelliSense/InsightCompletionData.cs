﻿using ICSharpCode.AvalonEdit.CodeCompletion;
using System.Collections.Generic;
using System.Windows.Media;
using SQLiteNavigator.Classes.Helpers;
using System.Windows.Controls;
using System.Xml.Linq;
using SQLiteNavigator.Controls;

namespace SQLiteNavigator.Classes.IntelliSense
{
    class InsightCompletionData: ICompletionData
    {
        private string _name;
        private IEnumerable<XElement> _data;
        private InsightWindowControl _content;

        public InsightCompletionData(string name, IEnumerable<XElement> data)
        {
            _name = name;
            _data = data;

            _content = new InsightWindowControl();
            _content.Fill(_name, _data);
        }

        #region ICompletionData Members

        public void Complete(ICSharpCode.AvalonEdit.Editing.TextArea textArea, ICSharpCode.AvalonEdit.Document.ISegment completionSegment, System.EventArgs insertionRequestEventArgs)
        {
            
        }

        public object Content
        {
            get { return _content; }
        }

        public object Description
        {
            get { throw new System.NotImplementedException(); }
        }

        public ImageSource Image
        {
            get { return ResourceHelper.GetResource("function"); }
        }

        public double Priority
        {
            get { throw new System.NotImplementedException(); }
        }

        public string Text
        {
            get { return _name; }
        }

        #endregion
    }
}
