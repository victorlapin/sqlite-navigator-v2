﻿using System;
using ICSharpCode.AvalonEdit.CodeCompletion;
using System.Windows.Media;
using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Document;
using SQLiteNavigator.Classes.Helpers;
using System.Windows.Media.Imaging;
using SQLiteNavigator.Controls;

namespace SQLiteNavigator.Classes.IntelliSense
{
    class TextCompletionData: ICompletionData
    {
        private readonly double _priority;
        private readonly string _header;
        private readonly string _text;
        private readonly string _image;
        private readonly string _type;

        public TextCompletionData(string header, string text, string image = "", string ftype = "", double priority = 0)
        {
            _header = header;
            _text = text;
            _priority = priority;
            _image = image;
            _type = ftype;
        }

        public ImageSource Image
        {
            get { return ResourceHelper.GetResource(_image); }
        }

        public string Text
        {
            get { return _text; }
        }

        public object Content
        {
            get { return _header; }
        }

        public object Description
        {
            get { return new CompletionDataTooltipControl(Image, _header, _type); }
        }

        public double Priority
        {
            get { return _priority; }
        }

        public void Complete(TextArea textArea, ISegment completionSegment, EventArgs insertionRequestEventArgs)
        {
            textArea.Document.Replace(completionSegment, Text);
        }
    }
}
