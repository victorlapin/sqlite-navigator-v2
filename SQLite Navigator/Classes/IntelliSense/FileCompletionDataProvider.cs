﻿using System;
using System.Collections.Generic;
using System.Linq;
using ICSharpCode.AvalonEdit.CodeCompletion;
using System.IO;
using System.Xml.Linq;
using System.Reflection;

namespace SQLiteNavigator.Classes.IntelliSense
{
    class FileCompletionDataProvider: ICompletionDataProvider
    {
        private static readonly Dictionary<string, IEnumerable<ICompletionData>> Data = new Dictionary<string, IEnumerable<ICompletionData>>();

        public IEnumerable<ICompletionData> GetData(string text, int position, string input, string highlightingName)
        {
            if (!Data.Keys.Contains(highlightingName))
            {
                var result = GetData(highlightingName);
                Data.Add(highlightingName, result);
            }
            if (input == " ")
                return Data[highlightingName];
            return new List<ICompletionData>();
        }

        private IEnumerable<ICompletionData> GetData(string highlightingName)
        {
            try
            {
                var result = new List<ICompletionData>();

                using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("SQLiteNavigator.keywords_sqlite.xml"))
                {
                    XDocument xml = XDocument.Load(stream);
                    var data = xml.Descendants("Entry").Select(x => new TextCompletionData(x.Attribute("Word").Value.ToLowerInvariant(), 
                        x.Attribute("Word").Value.ToLowerInvariant(), 
                        (x.Attribute("Image") != null) ? x.Attribute("Image").Value : String.Empty,
                        (x.Attribute("Description") != null) ? x.Attribute("Description").Value : String.Empty
                        )).Cast<ICompletionData>().ToList();
                    result.AddRange(data);
                }
                return result;
            }
            catch (Exception)
            {
                return new List<ICompletionData>();
            }
        }
    }
}
