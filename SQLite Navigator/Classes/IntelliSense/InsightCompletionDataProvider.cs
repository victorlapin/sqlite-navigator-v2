﻿using System;
using System.Collections.Generic;
using System.Linq;
using ICSharpCode.AvalonEdit.CodeCompletion;
using System.IO;
using System.Reflection;
using System.Xml.Linq;

namespace SQLiteNavigator.Classes.IntelliSense
{
    class InsightCompletionDataProvider : ICompletionDataProvider
    {
        private static readonly Dictionary<string, IEnumerable<ICompletionData>> Data = new Dictionary<string, IEnumerable<ICompletionData>>();

        public IEnumerable<ICompletionData> GetData(string text, int position, string input, string highlightingName)
        {
            if (input == "(")
            {
                int index = text.LastIndexOf(" ", position, position);
                if (index < 0) index = 0;
                string predicate = text.Substring(index, position - index - 1).Trim();

                return GetData(predicate.ToLowerInvariant());                
            }
            return new List<ICompletionData>();
        }

        private IEnumerable<ICompletionData> GetData(string predicate)
        {
            try
            {
                var result = new List<ICompletionData>();

                using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("SQLiteNavigator.keywords_sqlite.xml"))
                {
                    XDocument xml = XDocument.Load(stream);
                    var data = xml.Descendants("Param").Where(x => x.Parent.Attribute("Word").Value.ToLowerInvariant().Equals(predicate));
                    result.Add(new InsightCompletionData(predicate, data));
                }
                return result;
            }
            catch (Exception)
            {
                return new List<ICompletionData>();
            }
        }
    }
}
