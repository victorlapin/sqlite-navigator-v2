﻿using System.Collections.Generic;
using ICSharpCode.AvalonEdit.CodeCompletion;

namespace SQLiteNavigator.Classes.IntelliSense
{
    public class CompletionDataComparer: IComparer<ICompletionData>
    {
        public int Compare(ICompletionData x, ICompletionData y)
        {
            return (x.Text.CompareTo(y.Text));
        }
    }
}
