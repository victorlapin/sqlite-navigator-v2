﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSharpCode.AvalonEdit.CodeCompletion;
using System.Data;
using System.Data.SQLite;

namespace SQLiteNavigator.Classes.IntelliSense
{
    class SchemaCompletionDataProvider : ICompletionDataProvider
    {
        public IEnumerable<ICompletionData> GetData(string text, int position, string input, string highlightingName)
        {
            if (input.Equals("."))
            {
                int index = text.LastIndexOf(" ", position, position);
                if (index < 0) index = 0;
                string predicate = text.Substring(index, position - index - 1).Trim();
                return GetData(predicate);
            }
            else if (input.Equals(" "))
                return GetData(String.Empty);
            else
                return new List<ICompletionData>();
        }

        private IEnumerable<ICompletionData> GetData(string predicate)
        {
            List<ICompletionData> result = new List<ICompletionData>();
            if (Workbench.Self.Solution.Connection.State == System.Data.ConnectionState.Open)
            {
                DataTable data;
                if (predicate.Equals(String.Empty))
                {
                    data = Workbench.Self.Solution.Connection.GetSchema(SQLiteMetaDataCollectionNames.Catalogs);
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        DataRow row = data.Rows[i];
                        bool IsAttached = !(row.ItemArray[0].ToString().Equals("main") || row.ItemArray[0].ToString().Equals("temp"));
                        result.Add(new TextCompletionData(row.ItemArray[0].ToString(), row.ItemArray[0].ToString(),
                            IsAttached ? "catalog_attached" : "catalog",
                            IsAttached ? "attached database" : "catalog"));
                    }
                }
                else
                {
                    string[] info = new string[] { predicate, null, null, null };
                    data = Workbench.Self.Solution.Connection.GetSchema("Tables", info);
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        DataRow row = data.Rows[i];
                        bool IsSystem = row[3].ToString().ToLowerInvariant().Equals("system_table");
                        result.Add(new TextCompletionData(row.ItemArray[2].ToString(), row.ItemArray[2].ToString(), "table", 
                            IsSystem ? "system table" : "table"));
                    }
                    data = Workbench.Self.Solution.Connection.GetSchema(SQLiteMetaDataCollectionNames.Views, info);
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        DataRow row = data.Rows[i];
                        result.Add(new TextCompletionData(row.ItemArray[2].ToString(), row.ItemArray[2].ToString(), "view", "view"));
                    }
                }
            }

            return result;
        }
    }
}
