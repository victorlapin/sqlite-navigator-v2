﻿using ICSharpCode.AvalonEdit.CodeCompletion;

namespace SQLiteNavigator.Classes.IntelliSense
{
    interface ICompletionWindowResolver
    {
        CompletionWindowBase Resolve();
    }
}
