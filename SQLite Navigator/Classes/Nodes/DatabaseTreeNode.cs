﻿using System;

namespace SQLiteNavigator.Classes.Nodes
{
    class DatabaseTreeNode: TreeNode
    {
        public DatabaseTreeNode(string Text)
            : base("database", Text)
        {
            LazyLoading = true;
            this.Click += new EventHandler(DefaultClickHandler);
            this.Tag = Text;
        }

        protected override void LoadChildren()
        {
            base.LoadChildren();
            this.Children.Add(new CatalogFolderTreeNode());            
        }
    }
}
