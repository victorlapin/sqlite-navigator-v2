﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQLiteNavigator.Classes.Nodes
{
    class QueryFolderTreeNode: FolderTreeNode
    {
        public QueryFolderTreeNode()
            : base("Queries")
        {
            
        }

        protected override void LoadChildren()
        {
            base.LoadChildren();
            foreach (string s in Workbench.Self.Solution.Queries)
            {
                this.Children.Add(new QueryTreeNode(s));
            }            
        }
    }
}
