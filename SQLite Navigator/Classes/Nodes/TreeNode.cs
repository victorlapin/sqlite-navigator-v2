﻿using System;
using ICSharpCode.TreeView;
using System.Windows.Media.Imaging;
using System.Text;
using System.IO;
using SQLiteNavigator.Classes.Helpers;
using System.Windows;
using SQLiteNavigator.ViewModels;

namespace SQLiteNavigator.Classes.Nodes
{
    public class TreeNode: SharpTreeNode
    {

        #region click event

        public void DefaultClickHandler(object sender, EventArgs e)
        {
            PropertiesViewModel view = Workbench.Self.FindTool<PropertiesViewModel>();
            if (view != null)
                view.ShowProperties(sender.GetType().Name, (TreeNode)sender);
        }

        private EventHandler clickHandler;
        public event EventHandler Click
        {
            add { clickHandler += value; }
            remove { clickHandler -= value; }
        }

        #endregion

        private string _text;
        private BitmapImage _glyph;

        public object Tag { get; set; }
        
        public string FullPath
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                TreeNode node = this;
                while (node != null)
                {
                    sb.Insert(0, Path.DirectorySeparatorChar);
                    sb.Insert(0, node.Text);
                    node = node.Parent as TreeNode;
                }
                return sb.ToString();
            }
        }

        public TreeNode(string Text)
        {
            SetText(Text);
        }

        public TreeNode(string GlyphSource, string Text): this(Text)
        {
            SetGlyph(GlyphSource);
        }

        public void SetText(string Text)
        {
            _text = Text;
            RaisePropertyChanged("Text");
        }

        protected void SetGlyph(string GlyphSource)
        {
            if (!String.IsNullOrEmpty(GlyphSource))
            {
                _glyph = ResourceHelper.GetResource(GlyphSource);
                RaisePropertyChanged("Icon");
            }
        }

        public void ReloadChildren()
        {
            LoadChildren();
        }

        #region Inherited members

        public override object Text
        {
            get
            {
                return _text;
            }
        }

        public override object Icon
        {
            get
            {
                return _glyph;
            }
        }

        public override bool ShowIcon
        {
            get
            {
                return true;
            }
        }

        protected override void LoadChildren()
        {
            try
            {
                Children.Clear();
                base.LoadChildren();
            }
            catch (NotSupportedException)
            {
            }
        }

        public override void ActivateItem(RoutedEventArgs e)
        {
            if (clickHandler != null)
                clickHandler(this, e);
            base.ActivateItem(e);
        }

        #endregion
    }
}
