﻿using System;

namespace SQLiteNavigator.Classes.Nodes
{
    class CatalogTreeNode: TreeNode
    {
        public CatalogTreeNode(string Text)
            : base("catalog", Text)
        {
            LazyLoading = true;
            this.Click += new EventHandler(DefaultClickHandler);
        }

        protected override void LoadChildren()
        {
            base.LoadChildren();
            this.Children.Add(new TableFolderTreeNode());
            this.Children.Add(new ViewFolderTreeNode());            
        }
    }
}
