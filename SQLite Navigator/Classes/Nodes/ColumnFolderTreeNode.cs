﻿using System;
using System.Collections.Generic;
using System.Data;
using SQLiteNavigator.Helpers;

namespace SQLiteNavigator.Classes.Nodes
{
    class ColumnFolderTreeNode: FolderTreeNode
    {
        public ColumnFolderTreeNode()
            : base("Columns")
        {

        }

        protected override void LoadChildren()
        {
            base.LoadChildren();

            string[] tableInfo = new string[] { TreeHelper.GetCatalog(this).Text.ToString(), null, this.Parent.Text.ToString(), null };
            DataTable dtColumns = Workbench.Self.Solution.Connection.GetSchema("Columns", tableInfo);

            for (int j = 0; j < dtColumns.Rows.Count; j++)
            {
                object[] colreader = dtColumns.Rows[j].ItemArray;
                List<string> colAttr = new List<string>();

                if ((bool)dtColumns.Rows[j]["primary_key"])
                    colAttr.Add("PK");
                string dataType = dtColumns.Rows[j]["data_type"].ToString();
                if (!dataType.Equals(String.Empty))
                {
                    if (dataType.Contains("varbinary") || dataType.Contains("varchar"))
                    {
                        if (!dtColumns.Rows[j]["character_maximum_length"].ToString().Equals("2147483647"))
                            colAttr.Add(String.Format("{1}({0})", dtColumns.Rows[j]["character_maximum_length"].ToString(), dataType));
                        else
                            colAttr.Add(String.Format("{0}(max)", dataType));
                    }
                    else
                        colAttr.Add(dataType);
                }

                if (!(bool)dtColumns.Rows[j]["is_nullable"])
                    colAttr.Add("not null");
                else
                    colAttr.Add("null");
                string cname = String.Format("{0} ({1})", dtColumns.Rows[j][3].ToString(), String.Join(", ", colAttr.ToArray()));

                ColumnTreeNode node = new ColumnTreeNode(cname, ((bool)dtColumns.Rows[j]["primary_key"]));
                node.Tag = dtColumns.Rows[j];
                this.Children.Add(node);
            }
        }
    }
}
