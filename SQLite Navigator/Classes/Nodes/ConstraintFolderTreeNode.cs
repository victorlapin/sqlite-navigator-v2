﻿using System.Data;
using SQLiteNavigator.Helpers;
using System.Data.SQLite;
using System;
using System.Text;

namespace SQLiteNavigator.Classes.Nodes
{
    class ConstraintFolderTreeNode: FolderTreeNode
    {
        public ConstraintFolderTreeNode()
            : base("Constraints")
        {

        }

        protected override void LoadChildren()
        {
            base.LoadChildren();

            string[] catInfo = new string[] { TreeHelper.GetCatalog(this).Text.ToString(), null, this.Parent.Text.ToString(), null };
            //checks
            DataTable dtTables = Workbench.Self.Solution.Connection.GetSchema(SQLiteMetaDataCollectionNames.Tables, catInfo);
            for (int z = 0; z < dtTables.Rows.Count; z++)
            {
                string[] def = dtTables.Rows[z]["table_definition"].ToString().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in def)
                    if (s.ToLowerInvariant().Contains("constraint ") && s.ToLowerInvariant().Contains("check "))
                    {
                        int startIndex = s.ToLowerInvariant().IndexOf("constraint") + 11;
                        int endIndex = s.IndexOf(" ", startIndex);
                        string name = s.Substring(startIndex,
                            endIndex - startIndex).Replace("[", String.Empty).Replace("]", String.Empty);
                        string expression = s.Substring(s.LastIndexOf("("), s.Length - s.LastIndexOf("(")).Trim();

                        this.Children.Add(new CheckTreeNode(name, expression));
                    }
            }
            //default values
            dtTables = Workbench.Self.Solution.Connection.GetSchema(SQLiteMetaDataCollectionNames.Columns, catInfo);
            StringBuilder sb = new StringBuilder();
            for (int z = 0; z < dtTables.Rows.Count; z++)
            {
                if (!dtTables.Rows[z]["column_default"].ToString().Equals(String.Empty))
                {
                    sb.Length = 0;
                    sb.Append("DF_");
                    sb.Append(this.Parent.Text);
                    sb.Append("_");
                    sb.Append(dtTables.Rows[z]["column_name"].ToString());
                    this.Children.Add(new DefaultValueTreeNode(sb.ToString()));
                }
            }            
        }
    }
}
