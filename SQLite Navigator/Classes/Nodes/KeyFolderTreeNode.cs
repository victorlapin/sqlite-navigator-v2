﻿using System;
using SQLiteNavigator.Helpers;
using System.Data;
using System.Data.SQLite;

namespace SQLiteNavigator.Classes.Nodes
{
    class KeyFolderTreeNode: FolderTreeNode
    {
        public KeyFolderTreeNode()
            : base("Keys")
        {

        }

        protected override void LoadChildren()
        {
            base.LoadChildren();

            string name;
            //primary keys
            string[] tableInfo = new string[] { TreeHelper.GetCatalog(this).Text.ToString(), null, this.Parent.Text.ToString(), null };
            DataTable dtKeys = Workbench.Self.Solution.Connection.GetSchema(SQLiteMetaDataCollectionNames.Indexes, tableInfo);
            for (int j = 0; j < dtKeys.Rows.Count; j++)
            {
                object[] keysReader = dtKeys.Rows[j].ItemArray;
                if ((bool)dtKeys.Rows[j]["primary_key"])
                {
                    name = dtKeys.Rows[j]["index_name"].ToString();
                    this.Children.Add(new PrimaryKeyTreeNode(name));
                }
            }
            //foreign keys
            dtKeys = Workbench.Self.Solution.Connection.GetSchema("ForeignKeys", tableInfo);
            //get schema definition for the foreign keys
            name = String.Empty;
            DataTable dtTables = Workbench.Self.Solution.Connection.GetSchema(SQLiteMetaDataCollectionNames.Tables, tableInfo);
            string[] def = new string[0];
            if (dtTables.Rows.Count > 0)
                def = dtTables.Rows[0]["table_definition"].ToString().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            for (int j = 0; j < dtKeys.Rows.Count; j++)
            {
                //looking for a right key name (without _0 suffixes)
                foreach (string s in def)
                    if (s.ToLowerInvariant().Contains("constraint ") && s.ToLowerInvariant().Contains("foreign key")
                        && s.Contains(dtKeys.Rows[j]["fkey_to_table"].ToString())
                        && s.Contains(dtKeys.Rows[j]["fkey_to_column"].ToString()))
                    {
                        int startIndex = s.ToLowerInvariant().IndexOf("constraint") + 11;
                        int endIndex = s.IndexOf(" ", startIndex);
                        name = s.Substring(startIndex,
                            endIndex - startIndex).Replace("[", String.Empty).Replace("]", String.Empty);
                    }

                if (String.IsNullOrEmpty(name))
                    name = dtKeys.Rows[j]["constraint_name"].ToString();
                ForeignKeyTreeNode node = new ForeignKeyTreeNode(name);
                node.Tag = dtKeys.Rows[j];
                this.Children.Add(node);
            }            
        }
    }
}
