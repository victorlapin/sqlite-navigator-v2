﻿using System.Data;
using System.Data.SQLite;
using System;

namespace SQLiteNavigator.Classes.Nodes
{
    class CatalogFolderTreeNode: FolderTreeNode
    {
        public CatalogFolderTreeNode()
            : base("Catalogs")
        {

        }

        protected override void LoadChildren()
        {
            base.LoadChildren();
            TreeNode node;
            DataTable dtCatalogs = Workbench.Self.Solution.Connection.GetSchema(SQLiteMetaDataCollectionNames.Catalogs);
            for (int i = 0; i < dtCatalogs.Rows.Count; i++)
            {
                string name = dtCatalogs.Rows[i].ItemArray[0].ToString();
                node = (dtCatalogs.Rows[i].ItemArray[0].ToString().Equals("main") ||
                    dtCatalogs.Rows[i].ItemArray[0].ToString().Equals("temp"))
                    ? new CatalogTreeNode(name)
                    : new AttachedCatalogTreeNode(name);
                node.Tag = dtCatalogs.Rows[i];
                this.Children.Add(node);
            }
        }
    }
}
