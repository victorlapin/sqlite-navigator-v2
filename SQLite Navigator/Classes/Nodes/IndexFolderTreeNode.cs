﻿using SQLiteNavigator.Helpers;
using System.Data;
using System.Data.SQLite;
using System;

namespace SQLiteNavigator.Classes.Nodes
{
    class IndexFolderTreeNode: FolderTreeNode
    {
        public IndexFolderTreeNode()
            : base("Indexes")
        {

        }

        protected override void LoadChildren()
        {
            base.LoadChildren();

            string[] tableInfo = new string[] { TreeHelper.GetCatalog(this).Text.ToString(), null, this.Parent.Text.ToString(), null };
            DataTable dtIndexes = Workbench.Self.Solution.Connection.GetSchema(SQLiteMetaDataCollectionNames.Indexes, tableInfo);
            for (int j = 0; j < dtIndexes.Rows.Count; j++)
            {
                object[] indexReader = dtIndexes.Rows[j].ItemArray;
                if (!(bool)indexReader[dtIndexes.Columns.IndexOf("primary_key")])
                {
                    string name = String.Empty;
                    try
                    {
                        name = ((bool)indexReader[dtIndexes.Columns.IndexOf("clustered")])
                            ? String.Format("{0} (clustered)", indexReader[dtIndexes.Columns.IndexOf("index_name")].ToString())
                            : indexReader[dtIndexes.Columns.IndexOf("index_name")].ToString();
                    }
                    catch
                    {
                        name = indexReader[dtIndexes.Columns.IndexOf("index_name")].ToString();
                    }
                    this.Children.Add(new IndexTreeNode(name));
                }
            }            
        }
    }
}
