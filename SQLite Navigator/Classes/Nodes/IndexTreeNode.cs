﻿using SQLiteNavigator.Helpers;
using System.Data;
using System.Data.SQLite;
using System.Collections.Generic;
using System;

namespace SQLiteNavigator.Classes.Nodes
{
    class IndexTreeNode: TreeNode
    {
        public IndexTreeNode(string Text)
            : base("index", Text)
        {
            LazyLoading = true;
        }

        protected override void LoadChildren()
        {
            base.LoadChildren();

            string[] tableInfo = new string[] { TreeHelper.GetCatalog(this).Text.ToString(), null, null, this.Text.ToString() };
            DataTable dtColumns = Workbench.Self.Solution.Connection.GetSchema(SQLiteMetaDataCollectionNames.IndexColumns, tableInfo);
            for (int j = 0; j < dtColumns.Rows.Count; j++)
            {
                if (dtColumns.Rows[j][8].ToString().Equals(this.Text))
                {
                    List<string> colAttr = new List<string>();
                    colAttr.Add(dtColumns.Rows[j][9].ToString().ToLowerInvariant());
                    colAttr.Add(dtColumns.Rows[j][10].ToString().ToLowerInvariant());
                    string name = String.Format("{0} ({1})", dtColumns.Rows[j][6], String.Join(", ", colAttr.ToArray()));
                    if (!this.Children.ContainsChildText(name))
                        this.Children.Add(new ColumnTreeNode(name));
                }
            }            
        }
    }
}
