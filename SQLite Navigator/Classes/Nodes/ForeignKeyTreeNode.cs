﻿using System;

namespace SQLiteNavigator.Classes.Nodes
{
    class ForeignKeyTreeNode: TreeNode
    {
        public ForeignKeyTreeNode(string Text)
            : base("fkey", Text)
        {
            this.Click += new EventHandler(DefaultClickHandler);
        }
    }
}
