﻿
namespace SQLiteNavigator.Classes.Nodes
{
    class TableTreeNode: TreeNode
    {
        public TableTreeNode(string Text)
            : base("table", Text)
        {
            LazyLoading = true;
        }

        protected override void LoadChildren()
        {
            base.LoadChildren();

            this.Children.Add(new ColumnFolderTreeNode());
            this.Children.Add(new KeyFolderTreeNode());
            this.Children.Add(new ConstraintFolderTreeNode());
            this.Children.Add(new TriggerFolderTreeNode());
            this.Children.Add(new IndexFolderTreeNode());            
        }
    }
}
