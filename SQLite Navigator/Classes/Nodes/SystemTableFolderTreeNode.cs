﻿using System.Data;
using System.Data.SQLite;
using SQLiteNavigator.Helpers;

namespace SQLiteNavigator.Classes.Nodes
{
    class SystemTableFolderTreeNode: FolderTreeNode
    {
        public SystemTableFolderTreeNode()
            : base("System Tables")
        {

        }

        protected override void LoadChildren()
        {
            base.LoadChildren();

            string[] catInfo = new string[] { TreeHelper.GetCatalog(this).Text.ToString(), null, null, null };
            DataTable dtTables = Workbench.Self.Solution.Connection.GetSchema(SQLiteMetaDataCollectionNames.Tables, catInfo);
            for (int z = 0; z < dtTables.Rows.Count; z++)
            {
                if (!dtTables.Rows[z][3].ToString().ToLowerInvariant().Equals("system_table"))
                    continue;
                string name = dtTables.Rows[z][2].ToString();
                this.Children.Add(new TableTreeNode(name));
            }            
        }
    }
}
