﻿
namespace SQLiteNavigator.Classes.Nodes
{
    class PrimaryKeyTreeNode: TreeNode
    {
        public PrimaryKeyTreeNode(string Text)
            : base("pkey", Text)
        {

        }
    }
}
