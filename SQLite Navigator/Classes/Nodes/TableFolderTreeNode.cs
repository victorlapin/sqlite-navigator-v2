﻿using System.Data;
using System.Data.SQLite;
using System;
using System.Collections.Specialized;
using SQLiteNavigator.Helpers;

namespace SQLiteNavigator.Classes.Nodes
{
    class TableFolderTreeNode: FolderTreeNode
    {
        public TableFolderTreeNode()
            : base("Tables")
        {

        }

        protected override void LoadChildren()
        {
            base.LoadChildren();
            this.Children.Add(new SystemTableFolderTreeNode());

            string[] catInfo = new string[] { TreeHelper.GetCatalog(this).Text.ToString(), null, null, null };
            DataTable dtTables = Workbench.Self.Solution.Connection.GetSchema(SQLiteMetaDataCollectionNames.Tables, catInfo);
            for (int z = 0; z < dtTables.Rows.Count; z++)
            {
                if (dtTables.Rows[z][3].ToString().ToLowerInvariant().Equals("system_table"))
                    continue;
                string name = dtTables.Rows[z][2].ToString();
                this.Children.Add(new TableTreeNode(name));
            }            
        }

        protected override void OnChildrenChanged(NotifyCollectionChangedEventArgs e)
        {
            base.OnChildrenChanged(e);
            this.SetText(
                (this.Children.Count > 1)
                ? String.Format("{0} ({1})", defaultText, this.Children.Count - 1)
                : defaultText
                );
        }
    }
}
