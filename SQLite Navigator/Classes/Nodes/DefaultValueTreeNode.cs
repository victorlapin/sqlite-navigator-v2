﻿
namespace SQLiteNavigator.Classes.Nodes
{
    class DefaultValueTreeNode: TreeNode
    {
        public DefaultValueTreeNode(string Text)
            : base("column_defaultvalue", Text)
        {

        }
    }
}
