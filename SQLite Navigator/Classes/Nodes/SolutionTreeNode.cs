﻿using System;

namespace SQLiteNavigator.Classes.Nodes
{
    class SolutionTreeNode: TreeNode
    {
        public SolutionTreeNode(string Text)
            : base("solution", String.Format("Solution '{0}'", Text))
        {
            this.Click += new EventHandler(DefaultClickHandler);
            this.Tag = Workbench.Self.Solution;
            this.LazyLoading = false;
            this.LoadChildren();
        }

        protected override void LoadChildren()
        {
            base.LoadChildren();
            this.Children.Add(new DatabaseTreeNode(Workbench.Self.Solution.ExplorerDbName));
            this.Children.Add(new QueryFolderTreeNode());
        }
    }
}
