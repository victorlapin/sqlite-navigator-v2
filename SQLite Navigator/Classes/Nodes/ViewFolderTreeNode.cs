﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;
using SQLiteNavigator.Helpers;

namespace SQLiteNavigator.Classes.Nodes
{
    class ViewFolderTreeNode: FolderTreeNode
    {
        public ViewFolderTreeNode()
            : base("Views")
        {

        }

        protected override void LoadChildren()
        {
            base.LoadChildren();

            string[] catInfo = new string[] { TreeHelper.GetCatalog(this).Text.ToString(), null, null, null };
            DataTable dtViews = Workbench.Self.Solution.Connection.GetSchema(SQLiteMetaDataCollectionNames.Views, catInfo);
            for (int z = 0; z < dtViews.Rows.Count; z++)
            {
                string name = dtViews.Rows[z][2].ToString();
                this.Children.Add(new ViewTreeNode(name));
            }            
        }
    }
}
