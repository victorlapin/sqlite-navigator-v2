﻿using System;

namespace SQLiteNavigator.Classes.Nodes
{
    class CheckTreeNode: TreeNode
    {
        public CheckTreeNode(string Text, string Expression)
            : base("check", Text)
        {
            this.Tag = Expression;
            this.Click += new EventHandler(DefaultClickHandler);
        }
    }
}
