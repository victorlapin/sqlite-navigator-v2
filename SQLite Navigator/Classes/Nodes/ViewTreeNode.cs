﻿
namespace SQLiteNavigator.Classes.Nodes
{
    class ViewTreeNode: TableTreeNode
    {
        public ViewTreeNode(string Text)
            : base(Text)
        {
            SetGlyph("view");
        }
    }
}
