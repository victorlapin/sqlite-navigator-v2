﻿using System.Collections.Specialized;
using System;

namespace SQLiteNavigator.Classes.Nodes
{
    class FolderTreeNode: TreeNode
    {
        protected string defaultText { get; private set; }

        public FolderTreeNode(string Text)
            : base("folder", Text)
        {
            LazyLoading = true;
            defaultText = Text;
        }

        protected override void OnChildrenChanged(NotifyCollectionChangedEventArgs e)
        {
            this.SetText(
                (this.Children.Count > 0)
                ? String.Format("{0} ({1})", defaultText, this.Children.Count)
                : defaultText
                );

            base.OnChildrenChanged(e);
        }
    }
}
