﻿
namespace SQLiteNavigator.Classes.Nodes
{
    class AttachedCatalogTreeNode: CatalogTreeNode
    {
        public AttachedCatalogTreeNode(string Text)
            : base(Text)
        {
            SetGlyph("catalog_attached");
        }
    }
}
