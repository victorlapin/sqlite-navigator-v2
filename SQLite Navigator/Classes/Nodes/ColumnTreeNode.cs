﻿using System;

namespace SQLiteNavigator.Classes.Nodes
{
    class ColumnTreeNode: TreeNode
    {
        public ColumnTreeNode(string Text, bool IsPrimaryKeyColumn = false)
            : base(Text)
        {
            SetGlyph(IsPrimaryKeyColumn ? "column_pkey" : "column");
            this.Click += new EventHandler(DefaultClickHandler);
        }
    }
}
