﻿using SQLiteNavigator.Helpers;
using System.Data;
using System.Data.SQLite;

namespace SQLiteNavigator.Classes.Nodes
{
    class TriggerFolderTreeNode: FolderTreeNode
    {
        public TriggerFolderTreeNode()
            : base("Triggers")
        {

        }

        protected override void LoadChildren()
        {
            base.LoadChildren();

            string[] tableInfo = new string[] { TreeHelper.GetCatalog(this).Text.ToString(), null, this.Parent.Text.ToString(), null };
            DataTable dtTriggers = Workbench.Self.Solution.Connection.GetSchema(SQLiteMetaDataCollectionNames.Triggers, tableInfo);
            for (int j = 0; j < dtTriggers.Rows.Count; j++)
            {
                string name = dtTriggers.Rows[j][3].ToString();
                this.Children.Add(new TriggerTreeNode(name));
            }            
        }
    }
}
