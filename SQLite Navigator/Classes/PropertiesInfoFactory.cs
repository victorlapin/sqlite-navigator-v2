﻿using System;
using SQLiteNavigator.Classes.Info;
using SQLiteNavigator.Metadata;
using System.Collections.Generic;
using System.Linq;
using SQLiteNavigator.Classes.Nodes;

namespace SQLiteNavigator.Classes
{   
    static class PropertiesInfoFactory
    {
        private static List<Lazy<IBaseInfo, IPropertiesInfoMetadata>> _source;

        public static void SetSource(Lazy<IBaseInfo, IPropertiesInfoMetadata>[] source)
        {
            _source = source.ToList();
        }
        
        public static IBaseInfo GetInfo(string contract, TreeNode argument)
        {
            if (argument == null)
                return null;

            foreach (Lazy<IBaseInfo, IPropertiesInfoMetadata> info in _source)
            {
                if (info.Metadata.Contract.Equals(contract))
                {
                    var result = info.Value.Clone();
                    result.FillInfo(argument);
                    return result;
                }
            }
            return null;
        }
    }
}
