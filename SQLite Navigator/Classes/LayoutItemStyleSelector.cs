﻿using System.Windows.Controls;
using SQLiteNavigator.ViewModels;
using System.Windows;

namespace SQLiteNavigator.Classes
{
    public class LayoutItemStyleSelector: StyleSelector
    {
        public override System.Windows.Style SelectStyle(object item, System.Windows.DependencyObject container)
        {
            if (item is ToolViewModel)
                return MainWindow.Instance.Resources["toolStyle"] as Style;
            else if (item is DocViewModel)
                return MainWindow.Instance.Resources["docStyle"] as Style;
            else
                return base.SelectStyle(item, container);
        }
    }
}
