﻿using System;
using SQLiteNavigator.Classes.Helpers;

namespace SQLiteNavigator.Classes
{
    public sealed class OutputEntry
    {
        public string GlyphSource { get; private set; }
        public string Text { get; private set; }
        public bool IsError { get; private set; }
        public string Date { get; private set; }

        public OutputEntry(string GlyphSource, string Text, bool IsError = false)
        {
            this.Date = DateTime.Now.ToLocalTime().ToString();
            this.GlyphSource = ResourceHelper.GetResourcePath(GlyphSource, 16);
            this.IsError = IsError;
            this.Text = Text;
        }
    }
}
