﻿using System;
using System.Windows.Data;
using SQLiteNavigator.ViewModels;

namespace SQLiteNavigator.Classes
{
    class ActiveDocumentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is DocViewModel)
                return value;

            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is DocViewModel)
                return value;

            return Binding.DoNothing;
        }
    }
}
