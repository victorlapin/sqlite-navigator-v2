﻿using System;
using SQLiteNavigator.Controls;
using SQLiteNavigator.Classes.Helpers;
using System.Windows.Media.Imaging;
using SQLiteNavigator.Metadata;
using Xceed.Wpf.AvalonDock.Layout;
using SQLiteNavigator.Classes;
using SQLiteNavigator.Classes.Nodes;

namespace SQLiteNavigator.ViewModels
{
    [ExportDockContent(Anchor = AnchorableShowStrategy.Right)]
    public class PropertiesViewModel: ToolViewModel
    {
        public PropertiesViewModel(): base("Properties")
        {
            IconSource = ResourceHelper.GetResource("properties");
            Content = new PropertiesControl();
        }

        public void ShowProperties(string contract, TreeNode argument)
        {
            (Content as PropertiesControl).SetObject(PropertiesInfoFactory.GetInfo(contract, argument));
        }
    }
}
