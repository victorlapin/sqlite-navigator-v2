﻿using System;
using SQLiteNavigator.Controls;
using System.Windows.Media.Imaging;
using SQLiteNavigator.Classes.Helpers;
using SQLiteNavigator.Metadata;
using Xceed.Wpf.AvalonDock.Layout;

namespace SQLiteNavigator.ViewModels
{
    [ExportDockContent(Anchor = AnchorableShowStrategy.Bottom)]
    public class OutputViewModel: ToolViewModel
    {
        public OutputViewModel()
            : base("Output")
        {
            IconSource = ResourceHelper.GetResource("output");
            base.Content = new OutputControl();
        }
    }
}
