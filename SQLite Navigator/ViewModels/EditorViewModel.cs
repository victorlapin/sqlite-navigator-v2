﻿using SQLiteNavigator.Metadata;
using Xceed.Wpf.AvalonDock.Layout;
using SQLiteNavigator.Classes.Helpers;
using System;
using System.Windows.Media.Imaging;
using SQLiteNavigator.Controls;
using System.IO;

namespace SQLiteNavigator.ViewModels
{
    [ExportDockContent(Anchor = AnchorableShowStrategy.Most)]
    public class EditorViewModel: DocViewModel
    {
        public EditorViewModel(): base()
        {
            Title = "SQL Query";
            IconSource = ResourceHelper.GetResource("sql");
            Content = new EditorControl();
        }

        public void Load(string filename)
        {
            if (File.Exists(filename))
                (Content as EditorControl).Load(filename);
            Title = Path.GetFileName(filename);
        }
    }
}
