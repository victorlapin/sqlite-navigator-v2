﻿using System.Windows.Input;
using SQLiteNavigator.Commands;

namespace SQLiteNavigator.ViewModels
{
    public class DocViewModel: PaneViewModel
    {
        public DocViewModel()
        {
            CloseCommand = new DocCloseCommand(this);
        }
        
        public ICommand CloseCommand { get; protected set; }
    }
}