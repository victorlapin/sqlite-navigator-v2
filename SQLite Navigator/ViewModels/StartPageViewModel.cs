﻿using SQLiteNavigator.Metadata;
using Xceed.Wpf.AvalonDock.Layout;
using SQLiteNavigator.Classes.Helpers;
using System;
using System.Windows.Media.Imaging;
using SQLiteNavigator.Controls;

namespace SQLiteNavigator.ViewModels
{
    [ExportDockContent(Anchor = AnchorableShowStrategy.Most)]
    public class StartPageViewModel: DocViewModel
    {
        public StartPageViewModel()
        {
            this.Title = "Start Page";
            IconSource = ResourceHelper.GetResource("startpage");
            Content = new StartPageControl();            
        }
    }
}
