﻿using System;
using System.Windows.Media.Imaging;
using SQLiteNavigator.Classes.Helpers;
using SQLiteNavigator.Controls;
using SQLiteNavigator.Metadata;
using Xceed.Wpf.AvalonDock.Layout;
using SQLiteNavigator.Classes;

namespace SQLiteNavigator.ViewModels
{
    [ExportDockContent(Anchor = AnchorableShowStrategy.Left)]
    public class ObjectExplorerViewModel: ToolViewModel
    {
        public ObjectExplorerViewModel(): base("Object Explorer")
        {
            IconSource = ResourceHelper.GetResource("objectexplorer");
            Content = new ObjectExplorerControl();
        }

        public void PopulateObjectExplorer()
        {
            try
            {
                (Content as ObjectExplorerControl).PopulateObjectExplorer();
            }
            catch (Exception e)
            {
                Workbench.Self.WriteError("Could not fill Object Explorer", e);
            }
        }

        public void ClearObjectExplorer()
        {
            (Content as ObjectExplorerControl).ClearObjectExplorer();
        }

        public object GetSelectedItem()
        {
            return (Content as ObjectExplorerControl).objectExplorer.SelectedItem;
        }
    }
}
