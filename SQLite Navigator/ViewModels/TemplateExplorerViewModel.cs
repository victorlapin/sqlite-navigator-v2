﻿using System;
using System.Windows.Media.Imaging;
using SQLiteNavigator.Classes.Helpers;
using SQLiteNavigator.Controls;
using SQLiteNavigator.Metadata;
using Xceed.Wpf.AvalonDock.Layout;

namespace SQLiteNavigator.ViewModels
{
    [ExportDockContent(Anchor = AnchorableShowStrategy.Right)]
    public class TemplateExplorerViewModel: ToolViewModel
    {
        public TemplateExplorerViewModel()
            : base("Template Explorer")
        {
            IconSource = ResourceHelper.GetResource("templateexplorer");
            base.Content = new TemplateExplorerControl();
        }        
    }
}
