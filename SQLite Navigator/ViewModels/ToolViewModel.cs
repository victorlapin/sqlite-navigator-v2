﻿using System.Windows.Input;
using SQLiteNavigator.Commands;

namespace SQLiteNavigator.ViewModels
{
    public class ToolViewModel: PaneViewModel
    {
        public ToolViewModel(string name)
        {
            Name = name;
            Title = name;
            HideCommand = new ToolHideCommand(this);
            CanClose = false;
        }

        public ICommand HideCommand { get; protected set; }
    }
}
