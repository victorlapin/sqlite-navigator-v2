﻿using ICSharpCode.TreeView;
using System.Windows;
using System.Windows.Controls;

namespace SQLiteNavigator
{
    public static class Extensions
    {
        public static bool ContainsChildText(this SharpTreeNodeCollection Collection, string Text)
        {
            foreach (SharpTreeNode node in Collection)
                if (node.Text.Equals(Text))
                    return true;
            return false;
        }

        public static void Toolbar_Loaded(object sender, RoutedEventArgs e)
        {
            ToolBar toolBar = sender as ToolBar;
            var overflowGrid = toolBar.Template.FindName("OverflowGrid", toolBar) as FrameworkElement;
            if (overflowGrid != null)
            {
                overflowGrid.Visibility = Visibility.Collapsed;
            }
        }
    }
}
