﻿using System;
using System.Windows.Controls;
using SQLiteNavigator.Classes;

namespace SQLiteNavigator.Controls
{
    public partial class OutputControl : UserControl
    {
        public OutputControl()
        {
            InitializeComponent();
        }
        
        private void UserControl_Initialized(object sender, EventArgs e)
        {
            outputView.ItemsSource = Workbench.Self.Output;
            Workbench.Self.OutputChanged += delegate
            {
                if (outputView.Items.Count > 0)
                    outputView.ScrollIntoView(outputView.Items[outputView.Items.Count - 1]);
            };
        }
    }
}