﻿using System;
using System.IO;
using System.Windows.Controls;
using SQLiteNavigator.Classes;
using SQLiteNavigator.Classes.Nodes;
using SQLiteNavigator.Commands;
using System.Windows.Input;
using ICSharpCode.TreeView;

namespace SQLiteNavigator.Controls
{
    public partial class TemplateExplorerControl : UserControl
    {
        public TemplateExplorerControl()
        {
            InitializeComponent();
        }
        
        private void UserControl_Initialized(object sender, EventArgs e)
        {
            FillTemplates();
        }

        #region Templates helper
        
        private string TemplatesFolder
        {
            get { return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates"); }
        }
        
        private void LoadTemplateText(TreeNode node)
        {
            string folder = Path.GetDirectoryName(node.FullPath.Replace("SQLite Navigator Templates", TemplatesFolder));
            string file = String.Format("{0}.sql", folder);
            try
            {
                if (File.Exists(file))
                    new NewQueryCommand().Execute(file);
            }
            catch (Exception ex)
            {
                Workbench.Self.WriteError("Could not load template", ex);
            }
        }

        private void FillTemplates()
        {
            tree.ItemsSource = null;
            TreeNode root = new TreeNode("folder", "SQLite Navigator Templates");
            if (Directory.Exists(TemplatesFolder))
            {
                FillFolder(root, TemplatesFolder);
                root.IsExpanded = true;
            }
            tree.Root = root;
        }

        private void FillFolder(TreeNode root, string path)
        {
            string[] folders = Directory.GetDirectories(path);
            foreach (string folder in folders)
            {
                TreeNode node = new TreeNode("folder", Path.GetFileName(folder));
                root.Children.Add(node);
                FillFolder(node, folder);
            }

            string[] files = Directory.GetFiles(path, "*.sql", SearchOption.TopDirectoryOnly);
            foreach (string file in files)
                root.Children.Add(new TreeNode("sql", Path.GetFileNameWithoutExtension(file)));
        }
        
        #endregion

        private void tree_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                if (e.MouseDevice.Captured is SharpTreeViewItem)
                {
                    var item = e.MouseDevice.Captured as SharpTreeViewItem;
                    LoadTemplateText(item.Node as TreeNode);
                }
        }
    }
}