﻿using System.Windows.Controls;
using SQLiteNavigator.Classes;
using System.Collections.ObjectModel;
using System;
using System.Windows.Input;
using SQLiteNavigator.Classes.Helpers;
using ICSharpCode.AvalonEdit.Folding;
using ICSharpCode.AvalonEdit.CodeCompletion;
using SQLiteNavigator.Classes.IntelliSense;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SQLite;
using System.Windows.Threading;
using System.ComponentModel;
using System.Windows;
using System.Collections.Generic;
using Xceed.Wpf.DataGrid;

namespace SQLiteNavigator.Controls
{
    /// <summary>
    /// Interaction logic for EditorControl.xaml
    /// </summary>
    public partial class EditorControl : UserControl
    {
        private ObservableCollection<OutputEntry> output = new ObservableCollection<OutputEntry>();
        private FoldingManager folding;
        private CompletionWindowBase _completionWindow;
        private BackgroundWorker worker = new BackgroundWorker();
        private DispatcherTimer timer = new DispatcherTimer();
        private GridLength height;

        #region Dependency Properties

        public static readonly DependencyProperty BusyProperty = DependencyProperty.Register(
          "IsBusy", typeof(Boolean), typeof(EditorControl), new PropertyMetadata(false));
        public Boolean IsBusy
        {
            get { return (Boolean)this.GetValue(BusyProperty); }
            set { this.SetValue(BusyProperty, value); }
        }

        public static readonly DependencyProperty IntellisenseProperty = DependencyProperty.Register(
          "IntellisenseEnabled", typeof(Boolean), typeof(EditorControl), new PropertyMetadata(true));
        public Boolean IntellisenseEnabled
        {
            get { return (Boolean)this.GetValue(IntellisenseProperty); }
            set { this.SetValue(IntellisenseProperty, value); }
        }

        public static readonly DependencyProperty WordWrapProperty = DependencyProperty.Register(
          "WordWrap", typeof(Boolean), typeof(EditorControl), new PropertyMetadata(false));
        public Boolean WordWrap
        {
            get { return (Boolean)this.GetValue(WordWrapProperty); }
            set { this.SetValue(WordWrapProperty, value); }
        }

        #endregion

        public EditorControl()
        {            
            InitializeComponent();
            tlbEditor.Loaded += new RoutedEventHandler(Extensions.Toolbar_Loaded);

            //set editor
            editor.SyntaxHighlighting = ResourceHelper.SqliteDefinition;
            if (ResourceHelper.SqlFoldingStrategy != null)
                folding = FoldingManager.Install(editor.TextArea);

            editor.TextArea.TextEntered += new TextCompositionEventHandler(TextArea_TextEntered);
            editor.TextArea.TextEntering += new TextCompositionEventHandler(TextArea_TextEntering);
            editor.TextChanged += new EventHandler(editor_TextChanged);

            //set output trace
            trace.ItemsSource = output;

            //set worker
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);

            //set timer
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += new EventHandler(timer_Tick);

            tabs.SelectionChanged += new SelectionChangedEventHandler(tabs_SelectionChanged);
        }

        void tabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabItem item = (tabs.SelectedItem as TabItem);
            if (item != null && item.Content is DataGridControl)
                rows.Text = String.Format("{0} rows", ((item.Content as DataGridControl).ItemsSource as DataView).Table.Rows.Count);
            else
                rows.Text = "0 rows";
        }

        void timer_Tick(object sender, EventArgs e)
        {
            DispatcherTimer timer = (sender as DispatcherTimer);
            TimeSpan span = ((TimeSpan)timer.Tag).Add(timer.Interval);
            timer.Tag = span;
            time.Text = String.Format("{0:00}:{1:00}:{2:00}", span.Hours, span.Minutes, span.Seconds);
            DoEvents.Push();
        }

        #region Worker

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //showing results
            if (e.Error == null && !e.Cancelled)
            {
                DataSet data = e.Result as DataSet;
                if (data != null)
                    foreach (DataTable table in data.Tables)
                    {
                        TabItem page = new TabItem();
                        page.HeaderTemplate = this.Resources["outputTabTemplate"] as DataTemplate;

                        DataGridControl dataGrid = new DataGridControl();
                        dataGrid.Style = this.Resources["gridResultStyle"] as Style;
                        dataGrid.View = new Xceed.Wpf.DataGrid.Views.TableView();
                        dataGrid.View.Style = this.Resources["tableViewStyle"] as Style;
                        dataGrid.View.FixedHeaders.Add(this.Resources["gridHeaders"] as DataTemplate);
                        dataGrid.ItemsSource = table.DefaultView;
                        page.Content = dataGrid;                        
                        tabs.Items.Insert(tabs.Items.IndexOf(tabMessages), page);                        
                        DoEvents.Push();
                    }
            }
            tabs.SelectedIndex = 0;
            tabs.RaiseEvent(new SelectionChangedEventArgs(TabControl.SelectionChangedEvent, new List<TabItem>(), new List<TabItem>()));

            timer.Stop();
            TimeSpan elapsed = (TimeSpan)timer.Tag;
            time.Text = String.Format("{0:00}:{1:00}:{2:00}", elapsed.Hours, elapsed.Minutes, elapsed.Seconds);
            timer.Tag = null;
            status.Text = "Ready";
            IsBusy = false;
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            output.Add(e.UserState as OutputEntry);
            if (trace.Items.Count > 0)
                trace.ScrollIntoView(trace.Items[trace.Items.Count - 1]);
            DoEvents.Push();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            DataSet data = e.Argument as DataSet;
            BackgroundWorker worker = (sender as BackgroundWorker);
            SQLiteConnection conn = Workbench.Self.Solution.Connection;
            string text = data.DataSetName;
            worker.ReportProgress(0, new OutputEntry("database_sql", "Preparing SQL statement..."));

            //eliminating comments and line breakers
            string builder = Regex.Replace(text,
            @"(--(.)*?$)|(//(.)*?$)", String.Empty,
            RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Multiline);
            builder = Regex.Replace(builder.Replace(Environment.NewLine, " "),
                @"(\/\*(.*?)\*\/)", String.Empty,
                RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Multiline);

            SQLiteTransaction tran = conn.BeginTransaction(IsolationLevel.Serializable);
            worker.ReportProgress(0, new OutputEntry("database_sql", "Transaction initialized"));
            string[] queriesRaw = builder.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                foreach (string query in queriesRaw)
                {
                    if (worker.CancellationPending)
                        throw new Exception("Canceled by user");
                    if (!query.Equals(String.Empty) && !query.Trim().ToLowerInvariant().Equals("end"))
                    {
                        using (SQLiteCommand command = new SQLiteCommand(query.Replace("(max)", "(2147483647)").Replace("\n", " ").Trim(), conn, tran))
                        {
                            string progress = (command.CommandText.Length > 50) ? command.CommandText.Substring(0, 50) : command.CommandText;
                            worker.ReportProgress(0, new OutputEntry("database_sql", String.Format("Executing: {0}", progress)));
                            string comtext = command.CommandText.ToLowerInvariant();
                            if (comtext.StartsWith("create trigger"))
                                command.CommandText += "; end";
                            if (!comtext.StartsWith("select") && !comtext.StartsWith("explain"))
                            {
                                int rowcount = command.ExecuteNonQuery();
                                if ((!comtext.StartsWith("create")) && (!comtext.StartsWith("alter"))
                                    && (!comtext.StartsWith("drop")))
                                    worker.ReportProgress(0, new OutputEntry("database_sql", String.Format("{0} row(s) affected", rowcount)));
                            }
                            else
                            {
                                DataTable table = data.Tables.Add();
                                SQLiteDataAdapter adapter = new SQLiteDataAdapter(command)
                                {
                                    MissingMappingAction = MissingMappingAction.Passthrough,
                                    MissingSchemaAction = MissingSchemaAction.AddWithKey,
                                    FillLoadOption = LoadOption.OverwriteChanges
                                };
                                adapter.Fill(table);
                                worker.ReportProgress(0, new OutputEntry("database_sql", String.Format("{0} row(s) fetched", table.Rows.Count)));
                            }
                            if (worker.CancellationPending)
                                throw new Exception("Canceled by user");
                        }
                    }
                }

                tran.Commit();
                worker.ReportProgress(0, new OutputEntry("ok", "Transaction committed"));
                e.Result = data;
            }
            catch(Exception ex)
            {
                tran.Rollback();
                worker.ReportProgress(0, new OutputEntry("error", ex.Message, true));
                worker.ReportProgress(0, new OutputEntry("error", "Transaction rolled back", true));
                throw ex;
            }
        }

        #endregion

        #region Editor

        void editor_TextChanged(object sender, EventArgs e)
        {
            if (folding != null)
                ResourceHelper.SqlFoldingStrategy.UpdateFoldings(folding, editor.Document);
        }

        void UpdateFoldings()
        {
            if (ResourceHelper.SqlFoldingStrategy != null)
                ResourceHelper.SqlFoldingStrategy.UpdateFoldings(folding, editor.Document);
        }

        void TextArea_TextEntered(object sender, TextCompositionEventArgs e)
        {
            if (IntellisenseEnabled)
            {
                ICompletionWindowResolver resolver = new CompletionWindowResolver(editor.Text, editor.CaretOffset, e.Text, editor);
                _completionWindow = resolver.Resolve();
            }
        }

        void TextArea_TextEntering(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Length > 0 && _completionWindow != null)
            {
                if (!Char.IsLetterOrDigit(e.Text[0]))
                {
                    if (_completionWindow is CompletionWindow)
                        (_completionWindow as CompletionWindow).CompletionList.RequestInsertion(e);
                }
            }
        }

        #endregion

        public void Load(string filename)
        {
            editor.Load(filename);
        }

        void ExecuteQuery()
        {
            if (!worker.IsBusy && Workbench.Self.Solution.Connection.State == System.Data.ConnectionState.Open
                && !String.IsNullOrEmpty(editor.Text))
            {
                IsBusy = true;
                status.Text = "Executing...";
                time.Text = "00:00:00";
                rows.Text = "0 rows";
                tabs.Items.Remove(tabMessages);
                tabs.Items.Clear();
                tabs.Items.Add(tabMessages);
                tabs.SelectedIndex = 0;
                ShowResultsPane();
                DoEvents.Push();
                timer.Tag = new TimeSpan(0, 0, 0);
                timer.Start();
                DataSet data = new DataSet();
                data.DataSetName = (editor.SelectionLength > 0) ? editor.SelectedText : editor.Text;                
                worker.RunWorkerAsync(data);                
            }
        }

        void CancelQuery()
        {
            if (worker.IsBusy) worker.CancelAsync();
        }

        private void UserControl_Initialized(object sender, EventArgs e)
        {
            HideResultsPane();
        }

        private void grid_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F5 && e.KeyboardDevice.Modifiers == ModifierKeys.None)
            {
                ExecuteQuery();
                e.Handled = true;
            }
            else if (e.Key == Key.R && e.KeyboardDevice.Modifiers == ModifierKeys.Control)
            {
                if (splitter.Visibility == System.Windows.Visibility.Collapsed)
                    ShowResultsPane();
                else
                    HideResultsPane();
                e.Handled = true;
            }
        }

        #region Visibility

        private void ShowResultsPane()
        {
            if (splitter.Visibility == System.Windows.Visibility.Collapsed)
            {
                splitter.Visibility = System.Windows.Visibility.Visible;
                rowResults.Height = height;
            }
        }

        private void HideResultsPane()
        {
            if (splitter.Visibility == System.Windows.Visibility.Visible)
            {
                height = rowResults.Height;
                rowResults.Height = new GridLength(0);
                splitter.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        #endregion

        #region Button clicks

        private void btnExecute_Click(object sender, RoutedEventArgs e)
        {
            ExecuteQuery();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            CancelQuery();
        }

        private void btnClearTrace_Click(object sender, RoutedEventArgs e)
        {
            output.Clear();
        }

        #endregion
    }
}
