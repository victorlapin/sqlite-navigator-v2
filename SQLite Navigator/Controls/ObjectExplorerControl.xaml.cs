﻿using System;
using System.Windows.Controls;
using SQLiteNavigator.Classes.Nodes;
using SQLiteNavigator.Classes;
using ICSharpCode.TreeView;
using System.Linq;
using SQLiteNavigator.Classes.Helpers;
using SQLiteNavigator.Commands;

namespace SQLiteNavigator.Controls
{
    public partial class ObjectExplorerControl : UserControl
    {
        public ObjectExplorerControl()
        {
            InitializeComponent();
        }

        public void PopulateObjectExplorer()
        {
            ClearObjectExplorer();

            TreeNode solutionNode = new SolutionTreeNode(Workbench.Self.Solution.Name);
            solutionNode.IsExpanded = true;
            objectExplorer.Root = solutionNode;

            try
            {
                objectExplorer.FocusNode(solutionNode);
            }
            catch (Exception e)
            {
                objectExplorer.Items.Clear();
                throw e;
            }
        }

        public void ClearObjectExplorer()
        {
            objectExplorer.ItemsSource = null;
        }

        private void objectExplorer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                TreeNode node = e.AddedItems[0] as TreeNode;
                node.ActivateItem(new System.Windows.RoutedEventArgs(e.RoutedEvent, node));
            }
        }

        private void objectExplorer_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            ctxMenu.Items.Clear();
            if ((sender as SharpTreeView).SelectedItem != null)
            {
                TreeNode node = ((sender as SharpTreeView).SelectedItem as TreeNode);
                string predicate = node.GetType().Name;

                foreach (var commandGroup in ContextMenuFactory.GetItems().Where(c =>
                    (c.Metadata.NodeClassName.Equals(predicate) || c.Metadata.NodeClassName.Equals("All"))).
                    OrderBy(c => c.Metadata.MenuOrder).GroupBy(c => c.Metadata.MenuCategory))
                {
                    if (ctxMenu.Items.Count > 0)
                        ctxMenu.Items.Add(new Separator());

                    foreach (var entry in commandGroup)
                    {
                        MenuItem item = new MenuItem()
                        {
                            Focusable = false,
                            Header = entry.Metadata.Header,
                            Command = CommandWrapper.Unwrap(entry.Value)
                        };
                        if (!string.IsNullOrEmpty(entry.Metadata.MenuIcon))
                        {
                            item.Icon = new Image
                            {
                                Width = 16,
                                Height = 16,
                                Source = ResourceHelper.GetResource(entry.Metadata.MenuIcon)
                            };
                        }

                        ctxMenu.Items.Add(item);
                    }
                }
            }

            if (ctxMenu.Items.Count == 0)
                e.Handled = true;
        }
    }
}
