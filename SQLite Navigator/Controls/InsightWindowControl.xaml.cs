﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Xml.Linq;
using System.Windows.Media.Imaging;
using SQLiteNavigator.Classes.Helpers;
using System.Windows;

namespace SQLiteNavigator.Controls
{
    /// <summary>
    /// Interaction logic for InsightWindowControl.xaml
    /// </summary>
    public partial class InsightWindowControl : UserControl
    {
        public InsightWindowControl()
        {
            InitializeComponent();
        }

        public void Fill(string name, IEnumerable<XElement> data)
        {
            lblName.Text = name;

            foreach (XElement node in data)
            {
                StackPanel param = new StackPanel()
                {
                    Orientation = Orientation.Horizontal,
                    Margin = new System.Windows.Thickness(2)
                };
                Image img = new Image()
                {
                    Source = ResourceHelper.GetResource(node.Attribute("Image").Value),
                    Margin = new System.Windows.Thickness(16,0,0,0)
                };
                param.Children.Add(img);
                TextBlock txt = new TextBlock()
                {
                    Margin = new System.Windows.Thickness(8, 0, 0, 0),
                    VerticalAlignment = System.Windows.VerticalAlignment.Center,
                    Text = node.Attribute("Description").Value
                };
                param.Children.Add(txt);
                
                pnlParams.Children.Add(param);
            }
        }
    }
}
