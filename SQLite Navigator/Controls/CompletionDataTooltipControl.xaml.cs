﻿using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace SQLiteNavigator.Controls
{
    /// <summary>
    /// Interaction logic for CompletionDataTooltipControl.xaml
    /// </summary>
    public partial class CompletionDataTooltipControl : UserControl
    {
        public CompletionDataTooltipControl(ImageSource image, string _name, string _type)
        {
            InitializeComponent();

            img.Source = image;
            fname.Text = _name;
            ftype.Text = _type;
        }
    }
}
