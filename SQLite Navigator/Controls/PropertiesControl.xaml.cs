﻿using System.Windows.Controls;

namespace SQLiteNavigator.Controls
{
    public partial class PropertiesControl : UserControl
    {
        public PropertiesControl()
        {
            InitializeComponent();
        }

        public void SetObject(object obj)
        {
            propertyGrid.SelectedObject = obj;
        }
    }
}
